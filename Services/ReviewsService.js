/**
 * Created by paras on 25/7/16.
 */

'use strict';

var Models = require('../Models');

//Get Users from DB
var getReviews = function (criteria, projection, options, callback) {
    Models.Reviews.find(criteria, projection, options, callback);
};


//Insert User in DB
var createReview = function(objToSave, callback) {
    new Models.Reviews(objToSave).save(callback)
};

//Update User in DB
var updateReviews = function(criteria, dataToSet, options, callback) {
    Models.Reviews.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteReview = function(criteria, callback) {
    Models.Reviews.findOneAndRemove(criteria, callback);
};


var getReviewsPopulate = function(criteria, project, options,populateModelArr, callback){
    Models.Reviews.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
}

module.exports = {
    getReviews: getReviews,
    createReview: createReview,
    updateReviews: updateReviews,
    deleteReview: deleteReview,
    getReviewsPopulate: getReviewsPopulate
};


'use strict';

var Models = require('../Models');

var getCategories= function(criteria,projection,option,callback){
    Models.Categories.find(criteria,projection,option,callback);
};

var gettiming= function(criteria,projection,option,callback){
    Models.Timings.find(criteria,projection,option,callback);
};


var createCategories= function (data,callback) {
   new Models.Categories(data).save(callback);
};

var deleteCategories= function (criteria,callback) {
    Models.Categories.remove(criteria,callback)
};
var updateCategories= function (criteria, dataToSet, options, callback) {
    Models.Categories.update(criteria, dataToSet, options, callback);
};

var getCategoryWithPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Categories.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var createTiming= function (data,callback) {
    new Models.Timings(data).save(callback);
};

module.exports = {
    getCategories: getCategories,
    createCategories:createCategories,
    deleteCategories:deleteCategories,
    updateCategories:updateCategories,
    getCategoryWithPopulate:getCategoryWithPopulate,
    createTiming:createTiming,
    gettiming:gettiming
    
};


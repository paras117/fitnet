'use strict';

var Models = require('../Models');

var getPosts= function(criteria,projection,option,callback){
    Models.Posts.find(criteria,projection,option,callback);
};

var createPosts= function (data,callback) {
   new Models.Posts(data).save(callback);
};

var deletePosts= function (criteria,callback) {
    Models.Posts.remove(criteria,callback)
};
var updatePosts= function (criteria, dataToSet, options, callback) {
    Models.Posts.update(criteria, dataToSet, options, callback);
};


var getPostWithPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Posts.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};



module.exports = {
    getPosts: getPosts,
    createPosts: createPosts,
    deletePosts:deletePosts,
    updatePosts:updatePosts,
    getPostWithPopulate:getPostWithPopulate
};


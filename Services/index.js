
module.exports = {
    CustomerService : require('./CustomerService'),
    CategoryService : require('./CategoryService'),
    PostService : require('./postService'),
    ReviewsService:  require('./ReviewsService'),
    ChatService:require('./ChatService')
};
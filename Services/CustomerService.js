'use strict';

var Models = require('../Models');

//Get Users from DB
var getCustomer = function (criteria, projection, options, callback) {
    Models.Customers.find(criteria, projection, options, callback);
};


//Get All Generated Codes from DB
var getAllGeneratedCodes = function (callback) {
    var criteria = {
        OTPCode : {$ne : null}
    };
    var projection = {
        OTPCode : 1
    };
    var options = {
        lean : true
    };
    Models.Customers.find(criteria,projection,options, function (err, dataAry) {
        if (err){
            callback(err)
        }else {
            var generatedCodes = [];
            if (dataAry && dataAry.length > 0){
                dataAry.forEach(function (obj) {
                    generatedCodes.push(obj.OTPCode.toString())
                });
            }
            callback(null,generatedCodes);
        }
    })
};

//Insert User in DB
var createCustomer = function (objToSave, callback) {
    new Models.Customers(objToSave).save(callback)
};

//Update User in DB
var updateCustomer = function (criteria, dataToSet, options, callback) {
    Models.Customers.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteCustomer = function (criteria, callback) {
    Models.Customers.findOneAndRemove(criteria, callback);
};



var getCustomerPopulate = function (criteria, project, options,populateModelArr, callback) {
    Models.Customers.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

var getReviewsPopulate = function(criteria, project, options,populateModelArr, callback){
    Models.Reviews.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
}

module.exports = {
    getCustomer: getCustomer,
    getAllGeneratedCodes: getAllGeneratedCodes,
    updateCustomer: updateCustomer,
    deleteCustomer: deleteCustomer,
    createCustomer: createCustomer,
    getCustomerPopulate:getCustomerPopulate,
    getReviewsPopulate:getReviewsPopulate
};


'use strict';

var Models = require('../Models');

var getUser= function(criteria,projection,option,callback){
    Models.Users.find(criteria,projection,option,callback);
};

var createUser= function (data,callback) {
   new Models.Users(data).save(callback);
};

var deleteUser= function (criteria,callback) {
    Models.Users.remove(criteria,callback)
};
var updateUser= function (criteria, dataToSet, options, callback) {
    Models.Users.update(criteria, dataToSet, options, callback);
};


module.exports = {
    getUser: getUser,
    createUser: createUser,
    deleteUser:deleteUser,
    updateUser:updateUser
};


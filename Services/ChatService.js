/**
 * Created by paras on 5/9/16.
 */

'use strict';

var Models = require('../Models');

//Get Users from DB
var getChats = function (criteria, projection, options, callback) {
    Models.PostComments.find(criteria, projection, options, callback);
};


//Insert User in DB
var createChat = function(objToSave, callback) {
    new Models.PostComments(objToSave).save(callback)
};

//Update User in DB
var updateChats = function(criteria, dataToSet, options, callback) {
    Models.PostComments.findOneAndUpdate(criteria, dataToSet, options, callback);
};

//Delete User in DB
var deleteChat = function(criteria, callback) {
    Models.PostComments.findOneAndRemove(criteria, callback);
};


var getChatPopulate = function(criteria, project, options,populateModelArr, callback){
    Models.PostComments.find(criteria, project, options).populate(populateModelArr).exec(function (err, docs) {
        console.log("...............err");
        if (err) {
            return callback(err, docs);
        }else{
            callback(null, docs);
        }
    });
};

module.exports = {
    getChats: getChats,
    createChat: createChat,
    updateChats: updateChats,
    deleteChat: deleteChat,
    getChatPopulate: getChatPopulate
};




var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST',
        path: '/api/user/addUser',
        handler: function (request, reply) {
            var details=request.payload;
            Controller.UserController.addUser(details,function (err,data) {
                if(err){
                    reply(UniversalFunctions.sendError(err));
                }
                else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                }
            })
        },
        config: {
            description: 'ADD USER',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    name : Joi.string().regex(/^[a-zA-Z]/).trim().required(),
                    email: Joi.string().email().required(),
                    password: Joi.string().min(5).trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/user/deleteUSer',
        handler: function (request, reply) {
            var details=request.payload;
            Controller.UserController.deleteUser(details,function (err,data) {
                if(err){
                    reply(UniversalFunctions.sendError(err));
                }
                else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.DELETED, data)).code(200);
                }
            })
        },
        config: {
            description: 'DELETE USER',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    email: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/user/getAllUsers',
        handler: function (request, reply) {
            Controller.UserController.getUsers(function (err,data) {
                if(err){
                    reply(UniversalFunctions.sendError(err));
                }
                else {
                    reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.CREATED, data)).code(201);
                }
            })
        },
        config: {
            description: 'GET ALL USERS',
            tags: ['api', 'user'],
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/api/user/updateUser',
        handler: function (request, reply) {
            var details = request.payload;
            
                Controller.UserController.updateUser(details,function (err,data) {
                    if(err){
                        reply(UniversalFunctions.sendError(err));
                    }
                    else {
                        reply(UniversalFunctions.sendSuccess(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.SUCCESS.UPDATED, data)).code(201);
                    }
                });
        },
        config: {
            description: 'UPDATE USERS',
            tags: ['api', 'user'],
            validate: {
                payload: {
                    name : Joi.string().regex(/^[a-zA-Z]/).trim(),
                    email: Joi.string().email().trim(),
                    accessToken:Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
];
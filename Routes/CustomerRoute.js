'use strict';


var Controller = require('../Controllers');


var UniversalFunctions = require('../Utils/UniversalFunctions');


var x = '<p></p>';
    x+='<p><span style="font-family: Arial, serif;"><span><strong>Terms of Service</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>Effective: September 31st, 2016</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>Welcome!</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We&rsquo;re delighted you&rsquo;ve decided to use FitNet and the services it provides. For the purpose of this everything FitNet offers will fall under &lsquo;Services&rsquo;.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We"ve drafted these Terms of Service (which we simply call the "Terms") so that you"ll know the rules that govern our relationship with you. Although we have tried our best to strip the legalese from the Terms, there are places where these Terms may still read like a traditional contract. There"s a good reason for that: These Terms do indeed form a legally binding contract between you and FitNet, So please read them carefully.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>By using the Services provided by FitNet, you agree to the Terms. Of course, if you don"t agree with them, then don"t use the Services.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>1. Who Can Use the Services</strong></span></span></p>';
x+='<p><a name="_GoBack"></a> <span style="font-family: Arial, serif;"><span>By using the Services, you state that you can form a binding contract with FitNet;</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>If you are using the Services on behalf of a business or some other entity, you state that you are authorized to grant all licenses set forth in these Terms and to agree to these Terms on behalf of the business or entity.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>2. Rights We Grant You</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>FitNet grants you a personal, worldwide, royalty-free, non-assignable, nonexclusive, revocable, and non-sublicensable license to access and use the Services. This license is for the sole purpose of letting you use and enjoy the Service benefits in a way that these Terms and our usage policies allow.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>Any software that we provide you may automatically download and install upgrades, updates, or other new features. You may be able to adjust these automatic downloads through your device"s settings.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>You may not copy, modify, distribute, sell, or lease any part of our Services, nor may you reverse engineer or attempt to extract the source code of that software, unless applicable laws prohibit these restrictions or you have our written permission to do so.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>3. Rights You Grant Us</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>While we are not required to do so, we may access, review, screen, and delete your content at any time and for any reason, including if we think your content violates these Terms or our Community Guidelines. You alone though remain responsible for the content you create, upload, post, send, or store through the Service.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We always love to hear constructive criticism from our users. But if you volunteer feedback or suggestions, just know that we can use your ideas without compensating you.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>4. The Content of Others</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>Much of the content on our Services is produced by users, publishers, and other third parties. Whether that content is posted publicly or sent privately, the content is the sole responsibility of the person or organization that submitted it. Although FitNet reserves the right to review all content that appears on the Services and to remove any content that violates these Terms, we do not necessarily review all of it. So we cannot&mdash;and do not&mdash;take responsibility for any content that others provide through the Services.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>Through these Terms and our&nbsp;Community Guidelines, we make clear that we do not want the Services put to bad use. But because we do not review all content, we cannot guarantee that content on the Services will always conform to our Terms or Guidelines.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>5. Privacy</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>Your privacy matters to us. You can learn how we handle your information when you use our Services by reading our&nbsp;privacy policy. We encourage you to give the privacy policy a careful look because, by using our Services, you agree that FitNet can collect, use, and transfer your information consistent with that policy.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>6. Respecting Other People"s Rights</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>FitNet respects the rights of others. And so should you. You therefore may not upload, post, send, or store content that:</span></span></p>';
x+='<ul>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>violates or infringes someone else"s rights of publicity, privacy, copyright, trademark, or other intellectual-property right;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>bullies, harasses, or intimidates;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>defames; or</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>spams or solicits FitNet users.</span></span></p>';
x+='</li>';
x+='</ul>';
x+='<p><span style="font-family: Arial, serif;"><span>You must also respect FitNet rights. These Terms do not grant you any right to:</span></span></p>';
x+='<ul>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>use branding, logos, designs, photographs, videos, or any other materials used in our Services;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>copy, archive, download, upload, distribute, syndicate, broadcast, perform, display, make available, or otherwise use any portion of the Services or the content on the Services except as set forth in these Terms;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>use the Services or any content on the Services for any commercial purposes without our consent.</span></span></p>';
x+='</li>';
x+='</ul>';
x+='<p><span style="font-family: Arial, serif;"><span>In short: You may not use the Services or the content on the Services in ways that are not authorized by these Terms. Nor may you help or enable anyone else in doing so.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>7. Respecting Copyright</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>FitNet honors the requirements set forth in the Digital Millennium Copyright Act. We therefore take reasonable steps to expeditiously remove from our Services any infringing material that we become aware of. And if FitNet becomes aware that one of its users has repeatedly infringed copyrights, we will take reasonable steps within our power to terminate the users account.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We make it easy for you to report suspected copyright infringement. If you believe that anything on the Services infringes a copyright that you own or control, please send an email to info.fitnetapp@gmail.com.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>If you file a notice with our Copyright Agent, it must comply with the following requirements:</span></span></p>';
x+='<ul>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>contain the physical or electronic signature of a person authorized to act on behalf of the copyright owner;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>identify the copyrighted work claimed to have been infringed;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>identify the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed, or access to which is to be disabled, and information reasonably sufficient to let us locate the material;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>provide your contact information, including your address, telephone number, and an email address;</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>provide a personal statement that you have a good-faith belief that the use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>provide a statement that the information in the notification is accurate and, under penalty of perjury, that you are authorized to act on behalf of the copyright owner.</span></span></p>';
x+='</li>';
x+='</ul>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>8. Safety</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We try hard to keep our Services a safe place for all users. But we cant guarantee it. Thats where you come in. By using the Services, you agree that:</span></span></p>';
x+='<ul>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not use the Services for any purpose that is illegal or prohibited in these Terms.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not use any robot, spider, crawler, scraper, or other automated means or interface to access the Services or extract other users information.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not use or develop any third-party applications that interact with the Services or other users content or information without our written consent.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not use the Services in a way that could interfere with, disrupt, negatively affect, or inhibit other users from fully enjoying the Services, or that could damage, disable, overburden, or impair the functioning of the Services.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not use or attempt to use another users account, username, or password without their permission.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not solicit login credentials from another user.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not post content that contains pornography, graphic violence, threats, hate speech, or incitements to violence.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not upload viruses or other malicious code or otherwise compromise the security of the Services.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not attempt to circumvent any content-filtering techniques we employ, or attempt to access areas or features of the Services that you are not authorized to access.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not probe, scan, or test the vulnerability of our Services or any system or network.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You will not encourage or promote any activity that violates these Terms.</span></span></p>';
x+='</li>';
x+='</ul>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>9. Data Charges and Mobile Phones</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>You are responsible for any mobile charges that you may incur for using our Services, data charges. If you"re unsure what those charges may be, you should ask your service provider before using the Services.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>10. Third-Party Services</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>If you use a service, feature, or functionality that is operated by a third party and made available through our Services (including Services we jointly offer with the third party), each party terms will govern the respective party relationship with you. FitNet is not responsible or liable for those third party terms or actions taken under the third party"s terms.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>11. Modifying the Services and Termination</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We"re relentlessly improving our Services and creating new ones all the time. That means we may add or remove features, products, or functionalities, and we may also suspend or stop the Services altogether. We may take any of these actions at any time, and when we do, we may not provide you with any notice beforehand.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>While we hope you remain a life long FitNet member, you can terminate these Terms at any time and for any reason by deleting your account.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>FitNet may also terminate these Terms with you at any time, for any reason, and without advanced notice. That means that we may stop providing you with any Services, or impose new or additional limits on your ability to use the Services. For example, we may deactivate your account due to prolonged inactivity, and we may reclaim your username at any time for any reason.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>12. Indemnity</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>You agree, to the extent permitted under applicable law, to indemnify, defend, and hold harmless FitNet, our directors, officers, employees, and affiliates from and against any and all complaints, charges, claims, damages, losses, costs, liabilities, and expenses (including attorneys fees) due to, arising out of, or relating in any way to: (a) your access to or use of the Services; (b) your content; and (c) your breach of these Terms.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>13. Disclaimers</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>We try to keep the Services up and running and free of annoyances. But we make no promises that we will succeed.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>THE SERVICES ARE PROVIDED "AS IS" AND "AS AVAILABLE" AND TO THE EXTENT PERMITTED BY APPLICABLE LAW WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT. IN ADDITION, WHILE FITNET ATTEMPTS TO PROVIDE A GOOD USER EXPERIENCE, WE DO NOT REPRESENT OR WARRANT THAT: (A) THE SERVICES WILL ALWAYS BE SECURE, ERROR-FREE, OR TIMELY; (B) THE SERVICES WILL ALWAYS FUNCTION WITHOUT DELAYS, DISRUPTIONS, OR IMPERFECTIONS; OR (C) THAT ANY FITNET CONTENT, USER CONTENT, OR INFORMATION YOU OBTAIN ON OR THROUGH THE SERVICES WILL BE TIMELY OR ACCURATE.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>FITNET TAKES NO RESPONSIBILITY AND ASSUMES NO LIABILITY FOR ANY CONTENT THAT YOU, ANOTHER USER, OR A THIRD PARTY CREATES, UPLOADS, POSTS, SENDS, RECEIVES, OR STORES ON OR THROUGH OUR SERVICES. YOU UNDERSTAND AND AGREE THAT YOU MAY BE EXPOSED TO CONTENT THAT MIGHT BE OFFENSIVE, ILLEGAL, MISLEADING, OR OTHERWISE INAPPROPRIATE, NONE OF WHICH FITNET WILL BE RESPONSIBLE FOR.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>15. Limitation of Liability</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>TO THE MAXIMUM EXTENT PERMITTED BY LAW, FITNET AND OUR EMPLOYEES, AFFILIATES, LICENSORS, AND SUPPLIERS WILL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, PUNITIVE, OR MULTIPLE DAMAGES, OR ANY LOSS OF PROFITS OR REVENUES, WHETHER INCURRED DIRECTLY OR INDIRECTLY, OR ANY LOSS OF DATA, USE, GOODWILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM: (A) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICES; (B) THE CONDUCT OR CONTENT OF OTHER USERS OR THIRD PARTIES ON OR THROUGH THE SERVICES; OR (C) UNAUTHORIZED ACCESS, USE, OR ALTERATION OF YOUR CONTENT, EVEN IF FITNET HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF CERTAIN DAMAGES, SO SOME OR ALL OF THE EXCLUSIONS AND LIMITATIONS IN THIS SECTION MAY NOT APPLY TO YOU.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>16. Severability</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>If any provision of these Terms is found unenforceable, then that provision will be severed from these Terms and not affect the validity and enforceability of any remaining provisions.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>17. Additional Terms for Specific Services</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>Given the breadth of our Services, we sometimes need to craft additional terms and conditions for specific Services. Those additional terms and conditions, which will be available with the relevant Services, then become part of your agreement with us if you use those Services.</span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>18. Final Terms</strong></span></span></p>';
x+='<ul>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>These Terms make up the entire agreement between you and FitNet, and supersede any prior agreements.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>These Terms do no create or confer any third-party beneficiary rights.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>If we do not enforce a provision in these Terms, it will not be considered a waiver.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>We reserve all rights not expressly granted to you.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>You may not transfer any of your rights or obligations under these Terms without our consent.</span></span></p>';
x+='</li>';
x+='<li>';
x+='<p><span style="font-family: Arial, serif;"><span>These Terms were written in English and to the extent the translated version of these Terms conflict with the English version, the English version will control.</span></span></p>';
x+='</li>';
x+='</ul>';
x+='<p><span style="font-family: Arial, serif;"><span><strong>Contact Us</strong></span></span></p>';
x+='<p><span style="font-family: Arial, serif;"><span>FitNet welcomes comments, questions, concerns, or suggestions. Please send feedback to us be sending an email to: info.fitnetapp@gmail.com</span></span></p>';
x+='<p><br /><br /></p>';
var Joi = require('joi');


module.exports = [

    {
        method: 'POST',
        path: '/api/customer/loginViaFacebook',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.loginCustomerViaFacebook(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    console.log("fb login........................................", data);
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Login Via Facebook For  Customer',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    name: Joi.string().trim(),
                    imageUrl: Joi.string().trim(),
                    facebookId: Joi.string().required(),
                    email: Joi.string().required(),
                    latitude: Joi.number().required(),
                    longitude: Joi.number().required(),
                    deviceType: Joi.string().required().valid(
                        [UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
                            UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID]),
                    deviceToken: Joi.string().required().trim(),
                    appVersion: Joi.string().required().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/customer/getAllCategory',
        handler: function (request, reply) {
            Controller.CustomerController.getCategory(function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'get all category and timing array',
            tags: ['api', 'customer'],
            validate: {
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/customer/createNewPost',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.createNewPost(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'create a new post',
            tags: ['api', 'customer'],
            validate: {
                payload: {
                    title:Joi.string().required().trim(),
                    description: Joi.string().required().trim(),
                    categoryId: Joi.string().required(),
                    categoryName: Joi.string().trim(),
                    accessToken: Joi.string().required().trim(),
                    lat: Joi.string().required().trim(),
                    long: Joi.string().required().trim(),
                    address: Joi.string().required().trim(),
                    timingId: Joi.string().required().trim(),
                    postDate: Joi.string().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/customer/updateDistance',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.updateDistance(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Update Distance in settings page',
            tags: ['api', 'customer', 'distance'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    distance: Joi.number().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/customer/accessTokenLogin',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.accessTokenLogin(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    console.log("access token login.................", data);
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Access Token Login',
            tags: ['api', 'customer', 'access token login'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getUserList',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getListOfCustomers(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get the list of users with blocked and unblocked status',
            tags: ['api', 'customer', 'get list of users'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getAllPost',
        handler: function (request, reply) {
            var payloadData = request.payload;

            console.log(".................payloadData..............", payloadData);

            Controller.CustomerController.getAllPost(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get the all post according to home filter',
            tags: ['api', 'customer', 'get all post'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    categoryId: Joi.array(),
                    lat: Joi.string().trim(),
                    long: Joi.string().trim(),
                    timingId: Joi.string().trim()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/insertHomeData',
        handler: function (request, reply) {
            var payloadData = request.payload;

            console.log("......home data.............", request.payload);

            Controller.CustomerController.insertHomeData(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'insert Home page Data',
            tags: ['api', 'customer', 'home page data'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    categoryId: Joi.array().required(),
                    lat: Joi.string().trim().required(),
                    long: Joi.string().trim().required(),
                    timingId: Joi.string().trim().required(),
                    addressString: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/blockOrUnBlockCustomer',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.blockUnblockCustomer(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Block/unblock a user',
            tags: ['api', 'customer', 'block or unblock a user'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    customerId: Joi.string().trim().required(),
                    status: Joi.string().required().valid(
                        ['True', 'False'])
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/logout',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.logoutCustomer(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Customer logout',
            tags: ['api', 'customer', 'Logout'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/updateProfile',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.updateProfile(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Update profile of customer',
            tags: ['api', 'customer', 'update profile'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    aboutMe: Joi.string().trim().required(),
                    fitness: Joi.array().required(),
                    timingId: Joi.string().trim().required(),
                    level: Joi.string().trim().required()

                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getProfileData',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getProfileData(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'get profile data',
            tags: ['api', 'customer', 'profile data'],
            /*validate: {
             payload: {
             accessToken: Joi.string().trim().required()
             },
             failAction: UniversalFunctions.failActionFunction
             },*/
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getSinglePostDetails',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getSinglePostDetails(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get the single post details',
            tags: ['api', 'customer', 'get all post'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    postId: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getAllReviews',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getAllReviews(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get All Reviews',
            tags: ['api', 'customer', 'all reviewss'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/accpectJob',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.accpectJob(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Accpect Job',
            tags: ['api', 'customer', 'accpect Job'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    jobId: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/updateImage',
        handler: function (request, reply) {
            Controller.CustomerController.updateImage(request.payload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });

        },
        config: {
            description: 'Customer Update profile || WARNING : Will not work from documentation, use postman instead',
            tags: ['api', 'customer'],
            payload: {
                maxBytes: 10485760,
                parse: true,
                output: 'file'
            },
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    profilePic: Joi.any()
                        .meta({swaggerType: 'file'})
                        .optional()
                        .description('image file')
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/giveCommentsOnReview',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.giveComments(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Give comments on reviews',
            tags: ['api', 'customer', 'comments on reviews'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    reviewId: Joi.string().trim().required(),
                    comments: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/listCommentsOfSingleReview',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.listComments(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List comments of a single review',
            tags: ['api', 'customer', 'list comments on reviews'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    reviewId: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getCurrentActivities',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getCurrentActivities(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get current activities of user',
            tags: ['api', 'customer', 'current activities of user'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getOnGoingActivities',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getOnGoingActivities(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get on going activities of user',
            tags: ['api', 'customer', 'on going activities of user'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getMapUsers',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getMapUsers(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get near by map users',
            tags: ['api', 'customer', 'nearBy'],
            /*validate: {
             payload: {
             accessToken: Joi.string().trim().required(),
             lat:Joi.string().trim().required(),
             long:Joi.string().trim().required(),
             mode:Joi.string().optional()
             },
             failAction: UniversalFunctions.failActionFunction
             },*/
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/endJob',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.endJob(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'End Job',
            tags: ['api', 'customer', 'end job'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    jobId: Joi.string().trim().required(),
                    rating: Joi.string().trim().required(),
                    comments: Joi.string().trim().required()
                },
                //    failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/updateWorkHomeLocation',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.updateWorkLocation(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Update work location',
            tags: ['api', 'customer', 'work location'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    latitude: Joi.string().trim().required(),
                    longitude: Joi.string().trim().required(),
                    address: Joi.string().trim().required(),
                    status: Joi.number().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/getWorkHomeLocations',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getHomeWorkLocations(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get home and work location',
            tags: ['api', 'customer', 'home location', 'work location'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },

    {
        method: 'POST',
        path: '/api/customer/getAllReviewsOfOtherUser',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getAllReviewsOfOtherUser(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Get All Reviews of other user',
            tags: ['api', 'customer', 'all reviewss of other user'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    userId: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/listChatOfPost',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.getChat(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'List Chat Messages of a post',
            tags: ['api', 'customer', 'chat messages of a post'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    postId: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },


    {
        method: 'POST',
        path: '/api/customer/sendChatMessage',
        handler: function (request, reply) {
            var payloadData = request.payload;
            Controller.CustomerController.sendChat(payloadData, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        },
        config: {
            description: 'Send Chat Message ',
            tags: ['api', 'customer', 'send chat message'],
            validate: {
                payload: {
                    accessToken: Joi.string().trim().required(),
                    postId: Joi.string().trim().required(),
                    message: Joi.string().trim().required()
                },
                failAction: UniversalFunctions.failActionFunction
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/api/customer/termsAndConditions',
        handler: function (request, reply) {
            reply(x);
        },
        config: {
            description: 'terms and conditions',
            tags: ['api', 'customer', 'send chat message'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];
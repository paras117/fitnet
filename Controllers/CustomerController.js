'use strict';

var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');

var UploadManager = require('../Lib/UploadManager');

var TokenManager = require('../Lib/TokenManager');

var NotificationManager = require('../Lib/NotificationManager');

var CodeGenerator = require('../Lib/CodeGenerator');

var moment = require('moment');

var geolib = require('geolib');


function clearDeviceTokenFromDB(token, cb) {
    if (token) {
        var criteria = {
            deviceToken: token
        };
        var setQuery = {
            $unset: {deviceToken: 1}
        };
        var options = {
            multi: true
        };
        Service.CustomerService.updateCustomer(criteria, setQuery, options, cb)
    } else {
        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    }
}


var loginCustomerViaFacebook = function (payloadData, callback) {
    var userFound = false;
    var accessToken = null;
    var successLogin = false;
    var updatedUserDetails = null;
    var customerId = null;
    var location = false;
    async.series([
        function (cb) {
            var criteria = {
                facebookId: payloadData.facebookId
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "serachTime",
                    match: {},
                    select: '_id name',
                    options: {lean: true}
                },
                {
                    path: "serachCategories",
                    match: {},
                    select: ' _id name',
                    options: {lean: true}
                },
            ];
            Service.CustomerService.getCustomerPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    userFound = result && result[0] || null;
                    if (result.length) {

                        if (result[0].workAddress != "" || result[0].homeAddress != "") {
                            location = true;
                        }
                    }
                    cb();
                }
            });

        },
        function (cb) {
            //validations
            if (!userFound) {

                payloadData.profilePicURL = {
                    original: payloadData.imageUrl,
                    thumbnail: payloadData.imageUrl
                };

                Service.CustomerService.createCustomer(payloadData, function (err, customerDataFromDB) {
                    if (err) {
                        if (err.code == 11000 && err.message.indexOf('customers.$phoneNo_1') > -1) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_EXIST);

                        } else if (err.code == 11000 && err.message.indexOf('customers.$email_1') > -1) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_ALREADY_EXIST);

                        } else {
                            cb(err)
                        }
                    } else {
                        customerId = customerDataFromDB._id;
                        console.log(customerId)
                        userFound = {_id: customerId};
                        cb();
                    }
                })
            } else {
                customerId = userFound._id;
                successLogin = true;
                cb();
            }
        },
        function (cb) {
            var criteria = {
                _id: userFound._id
            };
            if (payloadData.imageUrl) {
                var setQuery = {
                    appVersion: payloadData.appVersion,
                    deviceToken: payloadData.deviceToken,
                    deviceType: payloadData.deviceType,
                    profilePicURL: {
                        original: payloadData.imageUrl,
                        thumbnail: payloadData.imageUrl
                    }
                };
            }
            else {
                var setQuery = {
                    appVersion: payloadData.appVersion,
                    deviceToken: payloadData.deviceToken,
                    deviceType: payloadData.deviceType,
                };
            }

            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                updatedUserDetails = data;
                cb(err, data);
            });
        },
        function (cb) {
            var tokenData = {
                id: userFound._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })
        },
        function (cb) {
            var criteria = {
                facebookId: payloadData.facebookId
            };
            var projection = {
                accessToken: 0
            };
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "serachTime",
                    match: {},
                    select: '_id Timing',
                    options: {lean: true}
                },
                {
                    path: "serachCategories",
                    match: {},
                    select: ' _id name',
                    options: {lean: true}
                }
            ];
            Service.CustomerService.getCustomerPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    updatedUserDetails = result[0];
                    cb();
                }
            });
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {


            console.log("...................location........................", location);
            callback(null, {
                accessToken: accessToken,
                userDetails: updatedUserDetails,
                location: location
            });
        }
    });
};


var accessTokenLogin = function (payloadData, callback) {
    var userFound = false;
    var customerDetails;
    var accessToken;
    var location = false;
    async.series([
        function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                accessToken: 0
            };
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "serachTime",
                    match: {},
                    select: '_id Timing',
                    options: {lean: true}
                },
                {
                    path: "serachCategories",
                    match: {},
                    select: ' _id name',
                    options: {lean: true}
                }
            ];
            Service.CustomerService.getCustomerPopulate(criteria, projection, option, populate, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerDetails = result[0];
                        if (result[0].workAddress != "" || result[0].homeAddress != "") {
                            location = true;
                        }
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });

        },
        function (cb) {
            var tokenData = {
                id: customerDetails._id,
                type: UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
            };
            TokenManager.setToken(tokenData, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    if (output && output.accessToken) {
                        accessToken = output && output.accessToken;
                        cb();
                    } else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.ERROR.IMP_ERROR)
                    }
                }
            })
        }
    ], function (err, data) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                accessToken: accessToken,
                userDetails: customerDetails,
                location: location
            });
        }
    });
}


var updateCustomer = function (userPayload, userData, callback) {
    var dataToUpdate = {};
    var updatedUser = null;
    var samePhoneNo = false;
    var uniqueCode = null;
    var phoneNoUpdateRequest = false;
    var newCountryCode = null;
    if (!userPayload || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        if (userPayload.phoneNo && userPayload.countryCode) {
            samePhoneNo = (userData.phoneNo == userPayload.phoneNo);
            samePhoneNo = (userData.countryCode == userPayload.phoneNo);
        }
        if (userPayload.name && userPayload.name != '') {
            dataToUpdate.name = UniversalFunctions.sanitizeName(userPayload.name);
        }
        if (userPayload.deviceToken && userPayload.deviceToken != '') {
            dataToUpdate.deviceToken = userPayload.deviceToken;
        }
        if (userPayload.phoneNo && userPayload.countryCode &&
            userData.phoneNo == userPayload.phoneNo && userData.countryCode == userPayload.countryCode) {
            delete userPayload.phoneNo;
            delete userPayload.countryCode;
        }

        if (userPayload.phoneNo && userPayload.phoneNo != '') {
            dataToUpdate.newNumber = userPayload.phoneNo;
        }
        if (userPayload.countryCode && userPayload.countryCode != '') {
            newCountryCode = userPayload.countryCode;
        }
        if (userPayload.email && userPayload.email != '') {
            dataToUpdate.email = userPayload.email;
        }
        if (userPayload.language && (userPayload.language == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.EN
            || userPayload.language == UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.LANGUAGE.ES_MX)) {
            dataToUpdate.language = userPayload.language;
        }
        if (userPayload.profilePic && userPayload.profilePic.filename) {
            dataToUpdate.profilePicURL = {
                original: null,
                thumbnail: null
            }
        }


        if (Object.keys(dataToUpdate).length == 0) {
            callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOTHING_TO_UPDATE);
        } else {
            async.series([
                function (cb) {
                    //verify email address
                    if (dataToUpdate.email && !UniversalFunctions.verifyEmailFormat(dataToUpdate.email)) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Validate phone No
                    if (userPayload.phoneNo && userPayload.phoneNo.split('')[0] == 0) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO_FORMAT);
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Validate countryCode and phoneNo
                    if (userPayload.countryCode) {
                        if (userPayload.countryCode.lastIndexOf('+') == 0) {
                            if (!isFinite(userPayload.countryCode.substr(1))) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                            }
                            else if (!userPayload.phoneNo || userPayload.phoneNo == '') {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_NO_MISSING);
                            } else {
                                cb();
                            }
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                        }
                    } else if (userPayload.phoneNo) {
                        if (!userPayload.countryCode) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.COUNTRY_CODE_MISSING);
                        } else if (userPayload.countryCode && userPayload.countryCode.lastIndexOf('+') == 0) {
                            if (!isFinite(userPayload.countryCode.substr(1))) {
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                            } else {
                                cb();
                            }
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_COUNTRY_CODE);
                        }
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Check all empty values validations
                    if (userPayload.name && !dataToUpdate.name) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMPTY_VALUE)
                    } else if (dataToUpdate.name) {
                        UniversalFunctions.customQueryDataValidations('NAME', 'name', dataToUpdate.name, cb)
                    } else {
                        cb();
                    }

                },
                function (cb) {
                    //Check if profile pic is being updated
                    if (userPayload.profilePic && userPayload.profilePic.filename) {
                        UploadManager.uploadFileToS3WithThumbnail(userPayload.profilePic, userData.id, function (err, uploadedInfo) {
                            if (err) {
                                cb(err)
                            } else {
                                dataToUpdate.profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                                dataToUpdate.profilePicURL.thumbnail = uploadedInfo && uploadedInfo.thumbnail && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.thumbnail || null;
                                cb();
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (userPayload.countryCode && userPayload.phoneNo && !samePhoneNo) {
                        var criteria = {
                            countryCode: userPayload.countryCode,
                            phoneNo: userPayload.phoneNo
                        };
                        var projection = {};
                        var option = {
                            lean: true
                        };
                        Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                            if (err) {
                                cb(err)
                            } else {
                                if (result && result.length > 0) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.PHONE_ALREADY_EXIST)
                                } else {
                                    cb();
                                }
                            }
                        });
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (!samePhoneNo && userPayload.phoneNo) {
                        CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                            if (err) {
                                cb(err);
                            } else {
                                if (!numberObj || numberObj.number == null) {
                                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                                } else {
                                    uniqueCode = numberObj.number;
                                    cb();
                                }
                            }
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    if (newCountryCode && dataToUpdate.newNumber && !samePhoneNo) {
                        //Send SMS to User on the new number
                        UniversalFunctions.NotificationManager.sendSMSToUser(uniqueCode, newCountryCode, dataToUpdate.newNumber, function (err, data) {
                            dataToUpdate.OTPCode = uniqueCode;
                            dataToUpdate.newNumber = newCountryCode + '-' + dataToUpdate.newNumber;
                            phoneNoUpdateRequest = true;
                            cb();
                        })
                    } else {
                        cb();
                    }
                },
                function (cb) {
                    //Update User
                    var criteria = {
                        _id: userData._id
                    };
                    var setQuery = {
                        $set: dataToUpdate
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, updatedData) {
                        updatedUser = updatedData;
                        cb(err, updatedData)
                    })
                }
            ], function (err, result) {
                if (err) {
                    callback(err)
                } else {
                    callback(null, {
                        phoneNoUpdateRequest: phoneNoUpdateRequest,
                        userData: UniversalFunctions.deleteUnnecessaryUserData(updatedUser.toObject())
                    });
                }
            })
        }
    }
};


var resetPassword = function (payloadData, callback) {
    var customerObj = null;
    if (!payloadData || !payloadData.email || !payloadData.passwordResetToken || !payloadData.newPassword) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //Get User
                var criteria = {
                    email: payloadData.email
                };
                Service.CustomerService.getCustomer(criteria, {}, {lean: true}, function (err, userData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData && userData[0] || null;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (customerObj) {
                    if (customerObj.passwordResetToken != payloadData.passwordResetToken) {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_RESET_PASSWORD_TOKEN);
                    } else {
                        cb();
                    }
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                }
            },
            function (cb) {
                if (customerObj) {
                    var criteria = {
                        email: payloadData.email
                    };
                    var setQuery = {
                        password: UniversalFunctions.CryptData(payloadData.newPassword),
                        $unset: {passwordResetToken: 1}
                    };
                    Service.CustomerService.updateCustomer(criteria, setQuery, {}, function (err, userData) {
                        if (err) {
                            cb(err)
                        } else {
                            cb();
                        }
                    })
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};


var changePassword = function (queryData, userData, callback) {
    var userFound = null;
    if (!queryData.oldPassword || !queryData.newPassword || !userData) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var projection = {};
                var options = {
                    lean: true
                };
                Service.CustomerService.getCustomer(criteria, projection, options, function (err, data) {
                    if (err) {
                        cb(err)
                    } else {
                        if (data && data.length > 0 && data[0]._id) {
                            userFound = data[0];
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND)
                        }
                    }
                })
            },
            function (cb) {
                //Check Old Password
                if (userFound.password != UniversalFunctions.CryptData(queryData.oldPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INCORRECT_OLD_PASS)
                } else if (userFound.password == UniversalFunctions.CryptData(queryData.newPassword)) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.SAME_PASSWORD)
                } else {
                    cb();
                }
            },
            function (cb) {
                // Update User Here
                var criteria = {
                    _id: userFound._id
                };
                var setQuery = {
                    $set: {
                        firstTimeLogin: false,
                        password: UniversalFunctions.CryptData(queryData.newPassword)
                    }
                };
                var options = {
                    lean: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }

        ], function (err, result) {
            callback(err, null);
        })
    }
};


var verifyEmail = function (emailVerificationToken, callback) {
    if (!emailVerificationToken) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    } else {
        var userData = null;
        async.series([
            function (cb) {
                var criteria = {
                    emailVerificationToken: emailVerificationToken
                };
                var setQuery = {
                    $unset: {emailVerificationToken: 1}
                };
                var options = {new: true};
                Service.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        } else {
                            userData = updatedData;
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null, UniversalFunctions.deleteUnnecessaryUserData(userData.toObject()));
            }

        });
    }

};


var verifyOTP = function (queryData, userData, callback) {
    if (!queryData || !userData._id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
    } else {
        var newNumberToVerify = queryData.countryCode + '-' + queryData.phoneNo;
        async.series([
            function (cb) {
                //Check verification code :
                if (queryData.OTPCode == userData.OTPCode) {
                    cb();
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                }
            },
            function (cb) {
                //Check if phoneNo is same as in DB
                console.log('checking data', userData.newNumber)
                console.log('checking data', newNumberToVerify)
                if (userData.newNumber != newNumberToVerify) {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_PHONE_NO);
                } else {
                    cb();
                }

            },
            function (cb) {
                //trying to update customer
                var criteria = {
                    _id: userData.id,
                    OTPCode: queryData.OTPCode
                };
                var setQuery = {
                    $set: {phoneVerified: true},
                    $unset: {OTPCode: 1}
                };
                if (userData.newNumber) {
                    setQuery.$set.phoneNo = queryData.phoneNo;
                    setQuery.$set.countryCode = queryData.countryCode;
                    setQuery.$unset.newNumber = 1;
                }
                var options = {new: true};
                console.log('updating>>>', criteria, setQuery, options)
                Service.CustomerService.updateCustomer(criteria, setQuery, options, function (err, updatedData) {
                    console.log('verify otp callback result', err, updatedData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!updatedData) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_CODE)
                        } else {
                            cb();
                        }
                    }
                });
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback();
            }

        });
    }

};


var resendOTP = function (userData, callback) {
    /*
     Create a Unique 4 digit code
     Insert It Into Customer DB
     Send the 4 digit code via SMS
     Send Back Response
     */
    var phoneNo = userData.newNumber || userData.phoneNo;
    var countryCode = userData.countryCode;
    if (!phoneNo) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var uniqueCode = null;
        async.series([
            function (cb) {
                CodeGenerator.generateUniqueCode(4, UniversalFunctions.CONFIG.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER, function (err, numberObj) {
                    if (err) {
                        cb(err);
                    } else {
                        if (!numberObj || numberObj.number == null) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.UNIQUE_CODE_LIMIT_REACHED);
                        } else {
                            uniqueCode = numberObj.number;
                            cb();
                        }
                    }
                })
            },
            function (cb) {
                var criteria = {
                    _id: userData.id
                };
                var setQuery = {
                    $set: {
                        OTPCode: uniqueCode,
                        codeUpdatedAt: new Date().toISOString()
                    }
                };
                var options = {
                    lean: true
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, options, cb);
            }, function (cb) {
                //Send SMS to User
                NotificationManager.sendSMSToUser(uniqueCode, countryCode, phoneNo, function (err, data) {
                    cb();
                })
            }
        ], function (err, result) {
            callback(err, null);
        })
    }
};


var getResetPasswordToken = function (email, callback) {
    var generatedString = UniversalFunctions.generateRandomString();
    var customerObj = null;
    if (!email) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                //update user
                var criteria = {
                    email: email
                };
                var setQuery = {
                    passwordResetToken: UniversalFunctions.CryptData(generatedString)
                };
                Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, userData) {
                    console.log('update user', err, userData, !userData)
                    if (err) {
                        cb(err)
                    } else {
                        if (!userData || userData.length == 0) {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOT_FOUND);
                        } else {
                            customerObj = userData;
                            cb()
                        }
                    }
                })
            },
            function (cb) {
                if (customerObj) {
                    var variableDetails = {
                        user_name: customerObj.name,
                        password_reset_token: customerObj.passwordResetToken,
                        password_reset_link: 'http://eiya.mx:8000/documentation' //TODO change this to proper html page link
                    };
                    NotificationManager.sendEmailToUser('FORGOT_PASSWORD', variableDetails, customerObj.email, cb)
                } else {
                    cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR)
                }
            }
        ], function (err, result) {
            if (err) {
                callback(err)
            } else {
                callback(null, {password_reset_token: customerObj.passwordResetToken})//TODO Change in production DO NOT Expose the password
            }
        })
    }
};


var addNewAddress = function (payloadData, userData, callback) {
    if (!payloadData || !userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userDataToSend = null;
        async.series([
            function (cb) {
                payloadData.locationLongLat = [payloadData.locationLong, payloadData.locationLat];
                payloadData.customer = userData.id;
                Service.CustomerService.addAddress(payloadData, function (err, userAddData) {
                    if (err) {
                        cb(err)
                    } else {
                        cb()
                    }
                })
            },
            function (cb) {
                Service.CustomerService.getAddress({
                    customer: userData.id,
                    isDeleted: false
                }, {}, {lean: true}, function (err, userAddArray) {
                    if (err) {
                        cb(err)
                    } else {
                        userDataToSend = userAddArray;
                        cb();
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                callback(err)
            } else {
                callback(null, userDataToSend)
            }
        });

    }
};


var getAddress = function (userData, callback) {
    if (!userData || !userData.id) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        var userDataToSend = null;

        async.series([
            function (cb) {
                var criteria = {
                    customer: userData.id,
                    isDeleted: false
                };
                Service.CustomerService.getAddress(criteria, {}, {lean: true}, function (err, userAddArray) {
                    if (err) {
                        cb(err)
                    } else {
                        userDataToSend = userAddArray;
                        cb();
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                callback(err)
            } else {
                callback(null, userDataToSend)
            }
        });

    }

};


var removeAddress = function (addressId, userData, callback) {
    if (!userData || !userData.id || !addressId) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    } else {
        async.series([
            function (cb) {
                var criteria = {
                    isDeleted: false,
                    customer: userData.id,
                    _id: addressId
                };
                Service.CustomerService.updateAddress(criteria, {isDeleted: true}, {}, function (err, userAddArray) {
                    if (err) {
                        cb(err)
                    } else {
                        if (userAddArray) {
                            cb();
                        } else {
                            cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.ADDRESS_NOT_FOUND);
                        }
                    }
                })
            }
        ], function (err, results) {
            if (err) {
                callback(err)
            } else {
                callback(null, null)
            }
        });

    }

};


var getCategory = function (callback) {

    var temp = {};
    async.auto({
        getcategory: function (cb) {
            var query = {
                isDeleted: false
            };
            var projections = {
                _id: 1,
                name: 1,
                order: 1
            };

            var options = {sort: {order: 1}};

            Service.CategoryService.getCategories(query, projections, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    temp.categories = result;
                    cb(null);
                }
            })
        },
        getTiming: function (cb) {
            var query = {
                isDeleted: false
            };
            var projections = {
                creationDate: 0,
                _v: 0
            };


            var options = {sort: {order: 1}};

            Service.CategoryService.gettiming(query, projections, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {

                    for (var i = 0; i < result.length; i++) {
                        (function (i) {
                            result[i].name = result[i].Timing;
                            delete result[i].Timing;

                        }(i));
                    }
                    if (i == result.length) {
                        temp.timing = result;
                        cb(null);
                    }

                }
            })
        }


    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, temp);
        }
    })

};


var createNewPost = function (payloadData, callback) {
    console.log("dshbvdsvds",payloadData);
    var token;
    var data;
    var session;
    var categoryName = "";
    var sessionTime = "09:30 am";
    async.auto({
        getToken: function (cb) {
            tokenValue(payloadData.accessToken, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    token = result;
                    cb(null);
                }
            })
        },
        createPost: ['getToken', function (cb) {

            if(payloadData.categoryName){
                categoryName = payloadData.categoryName;
            }
            var obj = {
                customer: token[0]._id,
                locationLongLat: [payloadData.lat, payloadData.long],
                locationAddress: payloadData.address,
                description: payloadData.description,
                categories: payloadData.categoryId,
                timings: payloadData.timingId,
                categoryName: categoryName,
                title:payloadData.title,
                postDate:payloadData.postDate
            };
            Service.PostService.createPosts(obj, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    data = result;
                    cb(null)
                }
            })
        }],
        getSessionValue: ['getToken', function (cb) {
            var query = {
                _id: token[0]._id
            };
            var projections = {sessions: 1};
            var options = {lean: true};
            Service.CustomerService.getCustomer(query, projections, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    session = result[0].sessions || 0;
                    cb(null)
                }
            })
        }],
        updateSessionValue: ['getSessionValue', function (cb) {
            var query = {
                _id: token[0]._id
            };
            session = parseInt(session) + parseInt(1);

            var setData = {sessions: session}
            var options = {lean: true};
            Service.CustomerService.updateCustomer(query, setData, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })

        }], /*
         sessionInc:['getToken',function(cb){
         var query = {
         _id:token[0]._id
         };
         var setData = {
         $inc:{ sessions:1}
         };
         var options = {lean:true};
         Service.CustomerService.updateCustomer(query,setData,options,function(err,result){
         if(err){
         cb(err);
         }else{
         cb(null)
         }
         })
         }]*/
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, data)
        }
    })
}


var tokenValue = function (token, callback) {
    var query = {
        accessToken: token
    };
    var options = {lean: true};
    var projections = {
        _id: 1,
        distanceMeter:1
    };
    Service.CustomerService.getCustomer(query, projections, options, function (err, result) {
        console.log("errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", err, result);
        if (err) {
            callback(err);
        } else {
            callback(null, result);
        }
    })
}


var updateDistance = function (payloadData, callback) {
    var customerId;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        updateDistance: ['checkToken', function (cb) {
            var criteria = {
                _id: customerId
            };
            var setQuery = {
                distanceMeter: payloadData.distance
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                distance: payloadData.distance
            });
        }
    })

};


var getListOfCustomers = function (payloadData, callback) {

    var customers;
    var blockedCustomers = [];
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                blockedCustomers: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        blockedCustomers = result[0].blockedCustomers;
                        console.log(blockedCustomers);
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        getCustomers: ['checkToken', function (cb) {
            var criteria = {
                isDeleted: 0
            };
            var projection = {
                _id: 1,
                name: 1,
                isBlocked: 1,
                profilePicURL: 1

            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customers = result;
                        if (blockedCustomers.length) {
                            for (var i = 0; i < customers.length; i++) {
                                (function (i) {

                                    for (var j = 0; j < blockedCustomers.length; j++) {
                                        (function (j) {
                                            console.log("customer", customers[i]._id);
                                            console.log("blocked", blockedCustomers[j]);
                                            if (customers[i]._id.toString() == blockedCustomers[j].toString()) {
                                                console.log("inside this");
                                                customers[i].isBlocked = true;
                                                if (i == customers.length - 1 && j == blockedCustomers.length - 1) {
                                                    cb();
                                                }
                                            }
                                            else {
                                                if (i == customers.length - 1 && j == blockedCustomers.length - 1) {
                                                    cb();
                                                }
                                            }

                                        }(j))

                                    }
                                }(i))
                            }
                        }
                        else {
                            cb();
                        }
                    }
                    else {
                        customers = [];
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                users: customers
            });
        }
    })
};


var getAllPost = function (payloadData, callback) {
    var token;
    var data = null;
    var finalData = [];
    var timingName = "";
    var categoryName = "";
    async.auto({
        getUserId: function (cb) {
            tokenValue(payloadData.accessToken, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    token = result;
                    if (token.length) {
                        cb(null);
                    } else {
                        return callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN)
                    }
                }
            })
        },
        checkForAnyTiming:['getUserId',function(cb)
        {
            if(payloadData.timingId)
            {
                var query = {
                    isDeleted: false,
                    _id:payloadData.timingId
                };
                var projections = {
                    creationDate: 0,
                    _v: 0
                };


                var options = {sort: {order: 1}};

                Service.CategoryService.gettiming(query, projections, options, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        timingName = result[0].Timing;
                        cb();
                    }
                })
            }
            else{
                cb();
            }

        }],
        checkForAnyCategory:['checkForAnyTiming',function(cb)
        {
            if(payloadData.categoryId)
            {
                var query = {
                    _id: {$in: payloadData.categoryId},
                    isDeleted: false
                };
                var projections = {
                    _id: 1,
                    name: 1,
                    order: 1
                };

                var options = {sort: {order: 1}};

                Service.CategoryService.getCategories(query, projections, options, function (err, result) {
                    if (err) {
                        cb(err);
                    } else {
                        for(var i = 0 ; i < result.length ; i++)
                        {
                            if(result[i].name == "Any of the above")
                            {
                                categoryName = result[i].name;
                            }

                        }
                        cb();
                    }
                })
            }
            else{
                cb();
            }

        }],
        getPost: ['checkForAnyCategory', function (cb) {

                if(payloadData.timingId && payloadData.categoryId){

                    if(timingName == "Any" && categoryName == "Any of the above")
                    {
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            isDeleted: false,
                        }
                    }
                    else if(timingName == "Any" && categoryName != "Any of the above")
                    {
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            categories: {$in: payloadData.categoryId},
                            isDeleted: false,
                        }
                    }
                    else if(timingName != "Any" && categoryName == "Any of the above")
                    {
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            timings: payloadData.timingId,
                            isDeleted: false,
                        }
                    }
                    else {
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            timings: payloadData.timingId,
                            categories: {$in: payloadData.categoryId},
                            isDeleted: false,
                        }
                    }

                }
                else if(payloadData.categoryId){

                    if(categoryName == "Any of the above")
                    {
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id}
                        }
                    }
                    else{
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            categories: {$in: payloadData.categoryId},
                        }
                    }

                }
                else if(payloadData.timingId)
                {
                    if(timingName == "Any")
                    {
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            isDeleted: false,
                        }
                    }
                    else{
                        var query = {
                            customer: {$ne: token[0]._id},
                            "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                            timings: payloadData.timingId,
                            isDeleted: false,
                        }
                    }

                }
                else{
                    var query = {
                        customer: {$ne: token[0]._id},
                        "acceptUser.acceptedCustomer": {$ne: token[0]._id},
                        isDeleted: false,
                    }
                }


            var options = {lean: true};
            var projections = {
                _id: 1,
                title:1,
                description: 1,
                creationDate: 1,
                locationLongLat: 1,
                locationAddress: 1,
                timings: 1,
                categories: 1,
                customer: 1,
                acceptUser: 1,
                categoryName:1,
                sessionTime:1,
                postDate:1
            };
            var populateModel = [
                {
                    path: "customer",
                    match: {},
                    select: '_id name profilePicURL latitude longitude Rating aboutMe sessions level goodFor totalReviews ',
                    options: {lean: true}
                },
                {
                    path: "categories",
                    match: {},
                    select: '_id name',
                    options: {lean: true}
                },
                {
                    path: "timings",
                    match: {},
                    select: '_id Timing',
                    options: {lean: true}
                }
            ];

            Service.PostService.getPostWithPopulate(query, projections, options, populateModel, function (err, result) {
               // console.log("............err...................result", err, result);

                if (err) {
                    cb(err)
                } else {
                    if (!(result.length)) {
                        data = [];
                        cb(null)
                    } else {

                        if(payloadData.lat){
                            var len = result.length;
                            var origin = {"lat":Number(payloadData.lat),"long":Number(payloadData.long)};
                            for (var i = 0; i < len; i++) {
                                (function(i)
                                {
                                    var destination = {"long":result[i].locationLongLat[1],"lat":result[i].locationLongLat[0]};
                                    console.log(destination);
                                    var distance = UniversalFunctions.getDistanceBetweenPoints(origin,destination);
                                    if(distance < token[0].distanceMeter/1000)
                                    {
                                        var x  = result[i].postDate.split(" ");
                                        result[i].sessionTime = x[3] +" "+ x[4];
                                        var temp = result[i].acceptUser;


                                        console.log(",............................",result[i]);

                                        result[i].customer.categories = ["Swimming"];
                                        result[i].customer.timings = "Before Work";
                                        result[i].accpectUsers = temp.length;
                                        result[i].postDate = x[0] +" "+ x[1]+" "+x[2];
                                        finalData.push(result[i]);
                                        if (i == (len - 1)) {
                                            cb(null);
                                        }
                                    }
                                    else{
                                        console.log("greater");
                                        if (i == (len - 1)) {
                                            cb(null);
                                        }
                                    }

                                }(i))
                            }


                        }
                        else{
                            var len = result.length;
                            for (var i = 0; i < len; i++) {
                                (function (i) {

                                    var x  = result[i].postDate.split(" ");
                                    result[i].sessionTime = x[3] +" "+ x[4];
                                    var temp = result[i].acceptUser;
                                    result[i].customer.categories = ["Swimming"];
                                    result[i].customer.timings = "Before Work";
                                    result[i].accpectUsers = temp.length;
                                    result[i].postDate = x[0] +" "+ x[1]+" "+x[2];
                                    //   result[i].attendees = "0";
                                    if (i == (len - 1)) {
                                        finalData = result;
                                        cb(null);
                                    }
                                }(i));
                            }
                        }

                    }

                }
            })
        }],
        getDate: ['getPost', function (cb) {
            var len = finalData.length;
            var temp;

            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(finalData[i].creationDate);
                    var newDates = moment(new Date());
                    var minutes = dbDate.diff(newDates, 'minutes');
                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);

                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }

                    }

                    finalData[i].creationDate = temp;

                    if (i == (len - 1)) {
                        cb(null);
                    }

                  //  console.log(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
                }(i));
            }

        }]


    }, function (err, result) {
       // console.log("/////////////////err//////////////resuult............................", err, result);
        if (err) {
            callback(err);
        } else {
            callback(null, finalData);
        }
    })
};


var insertHomeData = function (payloadData, callback) {
    var token;
    async.auto({
        getId: function (cb) {
            tokenValue(payloadData.accessToken, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    token = result;
                    cb(null);
                }
            })
        },
        updateValue: ['getId', function (cb) {
            var query = {
                _id: token[0]._id
            };
            var SetData = {
                serachCategories: payloadData.categoryId,
                serachLatitude: payloadData.lat,
                serachLongitude: payloadData.long,
                serachTime: payloadData.timingId,
                serachAddress: payloadData.addressString
            }
            var options = {lean: true};
            Service.CustomerService.updateCustomer(query, SetData, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    cb(null);
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
}


var blockUnblockCustomer = function (payloadData, callback) {

    var customerId;
    var blockedCustomers;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
                blockedCustomers: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        blockedCustomers = result[0].blockedCustomers;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        updateBlockedCustomers: ['checkToken', function (cb) {
            if (payloadData.status == 'True') {
                blockedCustomers.push(payloadData.customerId);
            }
            else {
                console.log(blockedCustomers);
                blockedCustomers = blockedCustomers.toString();
                console.log("c id ....", payloadData.customerId);
                var index = blockedCustomers.indexOf(payloadData.customerId);
                console.log(index);
                blockedCustomers = blockedCustomers.split(",");
                if (index > -1) {
                    blockedCustomers.splice(index, 1);
                    console.log(blockedCustomers);
                }
            }

            var criteria = {
                _id: customerId
            };
            var setQuery = {
                blockedCustomers: blockedCustomers
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                status: payloadData.status
            });
        }
    })

}


var logoutCustomer = function (payloadData, callback) {
    var customerId;
    var token = "";
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        clearAccessToken: ['checkToken', function (cb) {

            var criteria = {
                _id: customerId
            };
            var setQuery = {
                accessToken: token
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {});
        }
    })
}


var updateProfile = function (payloadData, callback) {
    var customerId;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        updateProfile: ['checkToken', function (cb) {

            var criteria = {
                _id: customerId
            };
            var setQuery = {
                categories: payloadData.fitness,
                aboutMe: payloadData.aboutMe,

                timings: payloadData.timingId,
                level: payloadData.level,
                //goodFor:payloadData.goodFor
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {});
        }
    })
};


var getProfileData = function (payloadData, callback) {
    var customerId;
    var customerData;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        profileData: ['checkToken', function (cb) {
            var criteria = {
                _id: customerId
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "timings",
                    match: {},
                    select: {},
                    options: {lean: true}
                },
                {
                    path: "categories",
                    match: {},
                    select: {},
                    options: {lean: true}
                },
            ];
            Service.CustomerService.getCustomerPopulate(criteria, projection, option, populate, function (err, data) {
                customerData = data[0];
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                userDetails: customerData
            });
        }
    })

}


var getSinglePostDetails = function (payloadData, callback) {
    var details = null;
    async.auto({
        getPostDetails: function (cb) {
            var query = {_id: payloadData.postId}
            var options = {lean: true};
            var projections = {};
            var populate = [
                {
                    path: "customer",
                    match: {},
                    select: '_id name Rating  profilePicURL aboutMe',
                    options: {lean: true}
                },
                {
                    path: "categories",
                    match: {},
                    select: '_id name',
                    options: {lean: true}
                },
                {
                    path: "timings",
                    match: {},
                    select: '_id name',
                    options: {lean: true}
                },
                {
                    path: "acceptUser.acceptedCustomer",
                    match: {},
                    select: '_id name Rating  profilePicURL aboutMe',
                    options: {lean: true}
                },
            ];

            Service.PostService.getPostWithPopulate(query, projections, options, populate, function (err, result) {
                if (err) {
                    cb(null);
                } else {

                    var temp = result[0].acceptUser;
                    result[0].acceptUserlength = temp.length

                    details = result;
                    cb(null);
                }
            })
        }
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, details);
        }
    })
};


var updateProfilePic = function (payloadData, profilePic, callback) {
    var customerId;
    var profileUrl;
    var customerDetails;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        profilePic: ['checkToken', function (cb) {
            UploadManager.uploadFile(profilePic, customerId, 0, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        profileUrl = result;
                    }
                }
            })
        }],
        uploadImage: ['profilePic', function (cb) {
            var criteria = {
                _id: customerId
            };
            var setQuery = {
                profilePicURL: profileUrl,
            };
            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                customerDetails = data;
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                userDetails: customerDetails
            });
        }
    })

};


var getAllReviews = function (payloadData, callback) {
    var customerId;
    var reviewsData = [];
    var customerDetails;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
                profilePicURL: 1,
                name: 1,
                Rating: 1,
                totalReviews: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        customerDetails = result[0]
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        reviewData: ['checkToken', function (cb) {
            var criteria = {
                toCustomer: customerId,
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "fromCustomer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                },
                {
                    path: "comments.customer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                }
            ];
            Service.ReviewsService.getReviewsPopulate(criteria, projection, option, populate, function (err, data) {


                console.log("data", data);
                reviewsData = data;
                cb();
            });
        }],
        getDate: ['reviewData', function (cb) {
            var len = reviewsData.length;
            var temp;
            var temp2;
            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(reviewsData[i].creationDate);
                    var newDates = moment(new Date());
                    var minutes = dbDate.diff(newDates, 'minutes');
                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);
                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }
                    }

                    reviewsData[i].timeDifference = temp;

                    if(reviewsData[i].comments.length){
                        for(var j = 0 ; j < reviewsData[i].comments.length ; j++)
                        {
                            (function(j)
                            {

                                var dbDate2 = moment(reviewsData[i].comments[j].creationDate);
                                var newDates2 = moment(new Date());
                                var minutes2 = dbDate2.diff(newDates2, 'minutes');
                                minutes2 = Math.abs(minutes2);
                                if (minutes2 < 30) {


                                    minutes2 = parseInt(minutes2);
                                    temp2 = minutes2;
                                    temp2 += " minutes ago"
                                }
                                else if (minutes2 > 30) {
                                    minutes2 = Math.ceil(minutes2 / 60);
                                    if (minutes2 > 24) {
                                        minutes2 = Math.ceil(minutes2 / 24);
                                        temp2 = minutes2;
                                        temp2 += " days ago";
                                    }
                                    else {
                                        temp2 = minutes2;
                                        temp2 += " hour ago";
                                    }
                                }

                                reviewsData[i].comments[j].timeDifference = temp2;

                            }(j))

                        }
                    }

                    if (i == (len - 1)) {
                        cb(null);
                    }


                }(i));
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                userDetails: customerDetails,
                reviews: reviewsData
            });
        }
    })

};


var giveComments = function (payloadData, callback) {
    var customerId;
    var comments;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        insertComment: ['checkToken', function (cb) {
            var criteria = {
                _id: payloadData.reviewId
            };
            var setQuery = {
                $push: {
                    "comments": {"customer": customerId, "comment": payloadData.comments}
                }
            };

            Service.ReviewsService.updateReviews(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });
        }],
        reviewData: ['insertComment', function (cb) {
            var criteria = {
                _id:payloadData.reviewId
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "fromCustomer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                },
                {
                    path: "comments.customer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                }
            ];
            Service.ReviewsService.getReviewsPopulate(criteria, projection, option, populate, function (err, data) {


                console.log("data", data);
                comments = data[0].comments;
                cb();
            });
        }],
        getDate: ['reviewData', function (cb) {
            var len = comments.length;
            var temp;
            var temp2;
            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(comments[i].creationDate);
                    var newDates = moment(new Date());
                    var minutes = dbDate.diff(newDates, 'minutes');
                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);
                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }
                    }

                    comments[i].timeDifference = temp;


                    if (i == (len - 1)) {
                        cb(null);
                    }


                }(i));
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {comments:comments});
        }
    })

};


var listComments = function (payloadData, callback) {
    var customerId;
    var comments = [];
    var customerDetails;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        comments: ['checkToken', function (cb) {

            var criteria = {
                _id: payloadData.reviewId
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "comments.customer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                }
            ];
            Service.ReviewsService.getReviewsPopulate(criteria, projection, option, populate, function (err, data) {
                console.log(err)
                if (!data.length) {
                    comments = [];
                }
                else {
                    comments = data[0].comments;
                }

                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                comments: comments
            });
        }
    })

};


var getCurrentActivities = function (payloadData, callback) {
    var id;
    var details = {};
    async.auto({
        getId: function (cb) {
            var query = {accessToken: payloadData.accessToken};
            var options = {lean: true};
            var projections = {
                _id: 1
            };
            Service.CustomerService.getCustomer(query, projections, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    id = result[0]._id;
                    cb(null);
                }
            })
        },
        onGoingActivities: ['getId', function (cb) {
            var criteria = {
                "acceptUser.acceptedCustomer": id,
                "acceptUser.endTime": ""
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "timings",
                    match: {},
                    select: '_id Timing ',
                    options: {lean: true}
                },
                {
                    path: "categories",
                    match: {},
                    select: '_id name ',
                    options: {lean: true}
                },
                {
                    path: "customer",
                    match: {},
                    select: '_id name profilePicURL ',
                    options: {lean: true}
                },
                {
                    path: "acceptUser.acceptedCustomer",
                    match: {},
                    select: '_id name profilePicURL ',
                    options: {lean: true}

                }
            ];

            Service.PostService.getPostWithPopulate(criteria, projection, option, populate, function (err, data) {
                console.log(err);
                if (!(data.length)) {
                    details.onGoing = [];
                }
                else {
                    details.onGoing = data;
                }
                cb(err, data);
            });
        }],
        myActivity: ['getId', function (cb) {
            var criteria = {
                customer: id
            };
            var option = {lean: true};
            var projection = {};
            var populate = [
                {
                    path: "timings",
                    match: {},
                    select: '_id Timing ',
                    options: {lean: true}
                },
                {
                    path: "categories",
                    match: {},
                    select: '_id name ',
                    options: {lean: true}
                },

                {
                    path: "customer",
                    match: {},
                    select: '_id name profilePicURL ',
                    options: {lean: true}
                },

                /*
                 {
                 path: "acceptUser.acceptedCustomer",
                 match: {},
                 select: '_id name profilePicURL',
                 options: {lean: true}
                 }*/
            ];
            Service.PostService.getPostWithPopulate(criteria, projection, option, populate, function (err, data) {
                console.log(err);
                if (!data.length) {
                    details.myActivity = [];
                } else {
                    details.myActivity = data;
                }
                cb(err, data);
            });
        }],
        getDateOnGoing: ['onGoingActivities', function (cb) {
            var len = details.onGoing.length;
            var temp;

            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(details.onGoing[i].creationDate);
                    var newDates = moment(new Date());
                    var minutes = dbDate.diff(newDates, 'minutes');
                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);

                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }

                    }

                    details.onGoing[i].creationDate = temp;

                    if (i == (len - 1)) {
                        cb(null);
                    }

                    console.log(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
                }(i));
            }

        }],
        getDateMyActivity: ['myActivity', function (cb) {
            var len = details.myActivity.length;
            var temp;

            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(details.myActivity[i].creationDate);
                    var newDates = moment(new Date());
                    var minutes = dbDate.diff(newDates, 'minutes');
                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);

                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }

                    }

                    details.myActivity[i].creationDate = temp;

                    if (i == (len - 1)) {
                        cb(null);
                    }

                    console.log(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
                }(i));
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, details)
        }
    })
}


var getOnGoingActivities = function (payloadData, callback) {
    var customerId;
    var activities;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        onGoingActivities: ['checkToken', function (cb) {

            var criteria = {
                customer: customerId
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "timings",
                    match: {},
                    select: '_id Timing ',
                    options: {lean: true}
                },
                {
                    path: "categories",
                    match: {},
                    select: '_id name ',
                    options: {lean: true}
                },
                {
                    path: "acceptUser.acceptedCustomer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                }
            ];
            Service.PostService.getPostWithPopulate(criteria, projection, option, populate, function (err, data) {
                console.log(err)
                if (!data.length) {
                    activities = [];
                }
                else {
                    activities = data;
                }
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                activities: result
            });
        }
    })

}


var accpectJob = function (payloadData, callback) {
    var customerId;
    var session;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        accpectJob: ['checkToken', function (cb) {
            var data = {
                acceptedCustomer: customerId,
                acceptTime: moment.utc().format(),
                startTime: moment.utc().format(),
            };

            var query = {
                _id: payloadData.jobId
            };
            var setData = {
                $push: {acceptUser: data}
            };
            var options = {lean: true};
            Service.PostService.updatePosts(query, setData, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    cb(null)
                }
            })
        }],
        getSessionValue: ['accpectJob', function (cb) {
            var query = {
                _id: customerId
            }
            var projections = {sessions: 1}
            var options = {lean: true};
            Service.CustomerService.getCustomer(query, projections, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    session = result[0].sessions || 0;
                    cb(null)
                }
            })
        }],
        updateSessionValue: ['getSessionValue', function (cb) {
            var query = {
                _id: customerId
            };
            session = parseInt(session) + parseInt(1);

            var setData = {sessions: session}
            var options = {lean: true};
            Service.CustomerService.updateCustomer(query, setData, options, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    cb(null)
                }
            })

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null);
        }
    })
}


var updateImage = function (payloadData, callback) {

    console.log("...payloadData.....image.", payloadData);
//callback(null)

    var customerId;
    var temp;
    var profilePicURL = {};
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                console.log("////////////////////  checkToken//////////////////", err, result);
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        uploadImage: ['checkToken', function (cb) {
            if (payloadData.profilePic && payloadData.profilePic.filename) {
                UploadManager.uploadFile(payloadData.profilePic, customerId, "hello", function (err, uploadedInfo) {
                    if (err) {
                        cb(err)
                    } else {
                        console.log("uploaded info", uploadedInfo);
                        profilePicURL.original = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        profilePicURL.thumbnail = uploadedInfo && uploadedInfo.original && UniversalFunctions.CONFIG.awsS3Config.s3BucketCredentials.s3URL + uploadedInfo.original || null;
                        cb();
                    }
                })
            } else {
                cb();
            }
        }],
        updateLinkDb: ['uploadImage', function (cb) {
            var query = {
                _id: customerId
            }
            var options = {new: true};
            var setData = {
                profilePicURL: profilePicURL
            }
            Service.CustomerService.updateCustomer(query, setData, options, function (err, result) {

                console.log("......................................updateLink.........", err, result);
                if (err) {
                    cb(err);
                } else {
                    cb(null);
                }
            })
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, profilePicURL);
        }
    })
};


var getMapUsers = function (payloadData, callback) {
    var details;
    var myDetails;
    var users = [];
    var userDistance;
    async.auto({
        getUserDetails: function (cb) {
            var query = {};

            if(payloadData.mode){
                if(payloadData.mode == "HOME"){
                    var projections = {
                        _id: 1,
                        homeLatitude: 1,
                        homeLongitude: 1,
                        profilePicURL: 1,
                        name: 1
                    };
                }else{
                    var projections = {
                        _id: 1,

                        workLatitude:1,
                        workLongitude:1,
                       // latitude: 1,
                       // longitude: 1,
                        profilePicURL: 1,
                        name: 1
                    };
                }
            }else{
                payloadData.mode == "HOME"
            }

            var options = {
                lean: true
            };
            Service.CustomerService.getCustomer(query, projections, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    details = result;
                    cb(null);
                }
            })
        },
        myDetails: function (cb) {
            var query = {accessToken: payloadData.accessToken};
            var options = {lean: true};
            var projections = {distanceMeter: 1};
            Service.CustomerService.getCustomer(query, projections, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    if (result.length) {
                        userDistance = result[0].distanceMeter;
                        userDistance = userDistance * 1000;
                    } else {
                        userDistance = 5000 * 1000;
                    }
                    cb(null);
                }
            })

        },

        matchDistance: ['getUserDetails', 'myDetails', function (cb) {
            var len = details.length;

            if (len == 0) {
                cb(null);
            }

            var myLocation = {};

            myLocation.latitude = parseFloat(payloadData.lat);
            myLocation.longitude = parseFloat(payloadData.long);


            for (var i = 0; i < len; i++) {
                (function (i) {

                    console.log(".............myLocation........", myLocation);
                    console.log(".............details[i]........", details[i]);


                    if(payloadData.mode == "WORK"){
                        var distance = geolib.getDistance(myLocation,
                            {latitude: parseFloat(details[i].workLatitude), longitude: parseFloat(details[i].workLongitude)}
                        );
                    }else{
                        var distance = geolib.getDistance(myLocation,
                            {latitude: parseFloat(details[i].homeLatitude), longitude: parseFloat(details[i].homeLongitude)}
                        );
                    }


                    console.log("............distance....................", distance);
                    console.log("............ userDistance....................", userDistance);


                    if(distance <= userDistance) {
                        if(payloadData.mode == "WORK"){
                            details[i].latitude = details[i].workLatitude;
                            details[i].longitude = details[i].workLongitude;

                            delete  details[i].workLatitude
                            delete details[i].workLongitude
                            users.push(details[i]);
                        }else{
                            details[i].latitude = details[i].homeLatitude
                            details[i].longitude = details[i].homeLongitude

                            delete details[i].homeLatitude
                            delete details[i].homeLongitude
                            users.push(details[i]);
                        }



                    }

                    if (i == (len - 1)) {
                        cb(null);
                    }
                }(i));
            }
        }]
    }, function (err, result) {
        console.log("........err..............result...............", err);

        if (err) {
            callback(err);
        } else {
            callback(null, users);
        }
    })
};


var endJob = function (payloadData, callback) {
    var postUserId;
    var customerId;
    var rating;
    var reviews;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                console.log("custoomer id......................................", err, result);

                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        endJob1: ['checkToken', function (cb) {


            var criteria = {
                _id: payloadData.jobId,
                "acceptUser.acceptedCustomer": customerId
            };

            var setData = {
                $set: {
                    "acceptUser.$.endTime": moment.utc().format()
                }
            };

            var option = {
                multi: true
            };

            Service.PostService.updatePosts(criteria, setData, option, function (err, result) {
                console.log("..............err............reslt.......", err, result);
                cb(null);
            });
        }],
        getcustomerId: ['endJob1', function (cb) {
            var query = {_id: payloadData.jobId};
            var options = {lean: true};
            var projections = {
                customer: 1
            };
            Service.PostService.getPosts(query, projections, options, function (err, result) {
                if (err) {
                    cb(err);
                } else {
                    postUserId = result[0].customer;
                    cb(null);
                }
            })
        }],
        insertReviews: ['getcustomerId', function (cb) {
            var obj = {
                fromCustomer: customerId,
                review: payloadData.comments,
                rating: payloadData.rating,
                toCustomer: postUserId
            };
            Service.ReviewsService.createReview(obj, function (err, result) {
                console.log("...........insert review.......................", err, result);
                if (err) {
                    cb(err);
                } else {
                    cb(null);
                }
            })

        }],
        getReviewsAndRating: ['insertReviews', function (cb) {
            var criteria = {
                _id: postUserId
            };
            var projection = {
                Rating: 1,
                totalReviews: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                console.log("custoomer id......................................", err, result);

                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        reviews = result[0].totalReviews;
                        rating = result[0].Rating;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });

        }],
        updateTotalReviews: ['getReviewsAndRating', function (cb) {
            var criteria = {
                _id: postUserId
            };

            reviews = parseInt(reviews) + parseInt(1);


            if (rating != 0) {
                rating = parseInt((parseInt((rating * reviews)) + parseInt(payloadData.rating)) / reviews);
                console.log(rating);
            }
            else {
                rating = parseInt(payloadData.rating);
                console.log(rating);
            }


            var setQuery = {
                Rating: rating,
                totalReviews: reviews

            };

            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                cb();
            });

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null)
        }
    })

};


var updateWorkLocation = function (payloadData, callback) {
    var customerId;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        updateDistance: ['checkToken', function (cb) {
            var criteria = {
                _id: customerId
            };
            if (payloadData.status == 0) {
                var setQuery = {
                    homeLatitude: payloadData.latitude,
                    homeLongitude: payloadData.longitude,
                    homeAddress: payloadData.address
                };
            }
            else {
                var setQuery = {
                    workLatitude: payloadData.latitude,
                    workLongitude: payloadData.longitude,
                    workAddress: payloadData.address
                };
            }

            Service.CustomerService.updateCustomer(criteria, setQuery, {new: true}, function (err, data) {
                cb(err, data);
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                latitude: payloadData.latitude,
                longitude: payloadData.longitude,
                address: payloadData.address
            });
        }
    })
};


var getHomeWorkLocations = function (payloadData, callback) {
    var customerId;
    var workLatitude;
    var workLongitude;
    var workAddress;
    var homeLatitude;
    var homeLongitude;
    var homeAddress;
    var distanceMeter;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        getLocations: ['checkToken', function (cb) {
            var criteria = {
                _id: customerId
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        workLatitude = result[0].workLatitude;
                        workLongitude = result[0].workLongitude;
                        workAddress = result[0].workAddress;
                        homeLatitude = result[0].homeLatitude;
                        homeLongitude = result[0].homeLongitude;
                        homeAddress = result[0].homeAddress;
                        distanceMeter = result[0].distanceMeter;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                workLatitude: workLatitude,
                workLongitude: workLongitude,
                workAddress: workAddress,
                homeLatitude: homeLatitude,
                homeLongitude: homeLongitude,
                homeAddress: homeAddress,
                distanceMeter:distanceMeter

            });
        }
    })
};

var getAllReviewsOfOtherUser = function (payloadData, callback) {
    var reviewsData = [];
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
                profilePicURL: 1,
                name: 1,
                Rating: 1,
                totalReviews: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        reviewData: ['checkToken', function (cb) {
            var criteria = {
                toCustomer: payloadData.userId,
                isDeleted: false
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "fromCustomer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                },
                {
                    path: "comments.customer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                }
            ];
            Service.ReviewsService.getReviewsPopulate(criteria, projection, option, populate, function (err, data) {

                // for(var  i = 0 ; i < data.length ; i++)
                // {
                //     data[i].timeDifference = "30 mins";
                // }

                console.log("data", data);
                reviewsData = data;
                cb();
            });
        }],
        getDate: ['reviewData', function (cb) {
            var len = reviewsData.length;
            var temp;
            var temp2;

            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(reviewsData[i].creationDate);
                    var newDates = moment(new Date());
                    var minutes = dbDate.diff(newDates, 'minutes');
                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);

                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }

                    }
                    if(reviewsData[i].comments.length){
                        for(var j = 0 ; j < reviewsData[i].comments.length ; j++)
                        {
                            (function(j)
                            {

                                var dbDate2 = moment(reviewsData[i].comments[j].creationDate);
                                var newDates2 = moment(new Date());
                                var minutes2 = dbDate2.diff(newDates2, 'minutes');
                                minutes2 = Math.abs(minutes2);
                                if (minutes2 < 30) {


                                    minutes2 = parseInt(minutes2);
                                    temp2 = minutes2;
                                    temp2 += " minutes ago"
                                }
                                else if (minutes2 > 30) {
                                    minutes2 = Math.ceil(minutes2 / 60);
                                    if (minutes2 > 24) {
                                        minutes2 = Math.ceil(minutes2 / 24);
                                        temp2 = minutes2;
                                        temp2 += " days ago";
                                    }
                                    else {
                                        temp2 = minutes2;
                                        temp2 += " hour ago";
                                    }
                                }

                                reviewsData[i].comments[j].timeDifference = temp2;

                            }(j))

                        }
                    }
                    reviewsData[i].timeDifference = temp;

                    if (i == (len - 1)) {
                        cb(null);
                    }

                    console.log(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
                }(i));
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                reviews: reviewsData
            });
        }
    })

};


var getChat = function (payloadData, callback) {
    var chatsData = [];
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        chatData: ['checkToken', function (cb) {
            var criteria = {
                post: payloadData.postId
            };
            var projection = {};
            var option = {
                lean: true
            };
            var populate = [
                {
                    path: "comment.customer",
                    match: {},
                    select: '_id name profilePicURL',
                    options: {lean: true}
                }
            ];
            Service.ChatService.getChatPopulate(criteria, projection, option, populate, function (err, data) {

                if (data.length) {
                    // console.log("data", data[0].comment);
                    chatsData = data[0].comment;
                    cb();
                }
                else {
                    callback(null, {
                        chats: []
                    });
                }


            });
        }],
        getDate: ['chatData', function (cb) {

            cb();
            var len = chatsData.length;
            var temp;

            if (len == 0) {
                cb(null);
            }
            for (var i = 0; i < len; i++) {
                (function (i) {
                    var dbDate = moment(chatsData[i].creationDate);
                    var newDates = moment(new Date());

                    var minutes = dbDate.diff(newDates, 'minutes');


                    minutes = Math.abs(minutes);
                    if (minutes < 30) {
                        console.log("..............................minutes..................", minutes);

                        console.log("..............................minutes..................", minutes);

                        minutes = parseInt(minutes);
                        temp = minutes;
                        temp += " minutes ago"
                    }
                    else if (minutes > 30) {
                        minutes = Math.ceil(minutes / 60);

                        if (minutes > 24) {
                            minutes = Math.ceil(minutes / 24);
                            temp = minutes;
                            temp += " days ago";
                        }
                        else {
                            temp = minutes;
                            temp += " hour ago";
                        }

                    }

                    chatsData[i].timeDifference = temp;

                    if (i == (len - 1)) {
                        cb(null);
                    }

                    console.log(";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
                }(i));
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                chats: chatsData
            });
        }
    })

};


var sendChat = function (payloadData, callback) {
    var customerId;
    var name;
    var profilePic;
    var x;
    var previousChat = false;
    async.auto({
        checkToken: function (cb) {
            var criteria = {
                accessToken: payloadData.accessToken
            };
            var projection = {
                _id: 1,
                name: 1,
                profilePicURL: 1
            };
            var option = {
                lean: true
            };
            Service.CustomerService.getCustomer(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        customerId = result[0]._id;
                        name = result[0].name,
                            profilePic = result[0].profilePicURL;
                        cb();
                    }
                    else {
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
                    }
                }
            });
        },
        checkChat: ['checkToken', function (cb) {
            var criteria = {
                post: payloadData.postId
            };
            var projection = {
                post: 1
            };
            var option = {
                lean: true
            };
            Service.ChatService.getChats(criteria, projection, option, function (err, result) {
                if (err) {
                    cb(err)
                } else {
                    if (result.length) {
                        previousChat = true;
                        cb();
                    }
                    else {
                        cb();
                    }
                }
            });

        }],
        insertChat: ['checkChat', function (cb) {

            if (previousChat == true) {
                var criteria = {
                    post: payloadData.postId
                };
                var setQuery = {
                    $push: {
                        "comment": {
                            "customer": customerId,
                            "comment": payloadData.message,
                            "creationDate": moment.utc().format()
                        }
                    }
                };
                Service.ChatService.updateChats(criteria, setQuery, {new: true}, function (err, data) {
                    console.log(data);

                    x = data.comment[data.comment.length - 1].creationDate;

                    cb(err, data);
                });
            }
            else {
                var objectToSave = {
                    post: payloadData.postId,
                    comment: {
                        "customer": customerId,
                        "comment": payloadData.message,
                        "creationDate": moment.utc().format()
                    }

                };
                Service.ChatService.createChat(objectToSave, function (err, data) {
                    console.log(data)
                  x = data.comment[data.comment.length - 1].creationDate;
                    cb(err, data);
                });
            }

        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        } else {
            callback(null, {
                chats: {
                    comment: payloadData.message,
                    creationDate: x,
                    customer: {
                        "_id": customerId,
                        "name": name,
                        "profilePicURL": profilePic
                    },
                    "timeDifference": "0 minutes ago"
                }
            });
        }
    })

};



function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

module.exports = {
    addNewAddress: addNewAddress,
    getAddress: getAddress,
    removeAddress: removeAddress,
    verifyEmail: verifyEmail,
    verifyOTP: verifyOTP,
    resendOTP: resendOTP,
    loginCustomerViaFacebook: loginCustomerViaFacebook,
    getResetPasswordToken: getResetPasswordToken,
    changePassword: changePassword,
    resetPassword: resetPassword,
    updateCustomer: updateCustomer,
    getCategory: getCategory,
    updateDistance: updateDistance,
    accessTokenLogin: accessTokenLogin,
    getListOfCustomers: getListOfCustomers,
    blockUnblockCustomer: blockUnblockCustomer,
    getAllPost: getAllPost,
    tokenValue: tokenValue,
    insertHomeData: insertHomeData,
    updateProfile: updateProfile,
    logoutCustomer: logoutCustomer,
    getProfileData: getProfileData,
    createNewPost: createNewPost,
    getSinglePostDetails: getSinglePostDetails,
    updateProfilePic: updateProfilePic,
    getAllReviews: getAllReviews,
    accpectJob: accpectJob,
    updateImage: updateImage,
    giveComments: giveComments,
    listComments: listComments,
    getCurrentActivities: getCurrentActivities,
    getOnGoingActivities: getOnGoingActivities,
    getMapUsers: getMapUsers,
    endJob: endJob,
    updateWorkLocation: updateWorkLocation,
    getHomeWorkLocations: getHomeWorkLocations,
    getAllReviewsOfOtherUser: getAllReviewsOfOtherUser,
    getChat: getChat,
    sendChat: sendChat
};
var Service = require('../Services');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var async = require('async');
var addUser = function (data,callback) {
    console.log("data", data);
    var accessToken=0;
    var email=data.email;
    async.auto({
        validEmail:function (cb) {
            if(UniversalFunctions.verifyEmailFormat(email)){
                cb(null);
            }
            else{
                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
            }
        },
        checkEmail:['validEmail',function (cb) {
            var criteria = {
                email: email
            };
            var projection = {};
            var option = {
                lean: true
            };
            Service.UserService.getUser(criteria, projection, option, function (err, result) {
                if (err) {
                    console.log("err",err);
                    cb(err);
                } else {
                    console.log("vauds",result)
                    if(result.length){
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST);
                    }
                    else{
                        cb(null);
                    }
                }
            });
        }],
        generateAccessToken:['checkEmail',function (cb) {
            var data1=(email+new Date());
            UniversalFunctions.generateToken(data1, function (err, output) {
                if (err) {
                    cb(err);
                } else {
                    accessToken =output;
                    data.accessToken=accessToken;
                    cb(null);
                }
            })

        }],
        encryptPassword:['generateAccessToken',function (cb) {
            var pass=UniversalFunctions.CryptData(data.password);
            data.password=pass;
            cb(null);
        }],
        insertDB:['encryptPassword',function (cb) {
            console.log("data",data);
            Service.UserService.createUser(data, function (err, result) {
                if (err) {
                    console.log('err144-----', err);
                    cb(err);
                } else {
                    console.log("created",result);
                    cb(null);
                }
            });
        }]
    }, function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            callback(null,accessToken);
        }
    });

};

var deleteUser = function (data,callback) {
    var email=data.email;
    var criteria = {
        email:email
    };
    Service.UserService.deleteUser(criteria,callback);

};

var getUsers = function (callback) {
    var criteria = {};
    var projection = {};
    var option = {
        lean: true
    };
    Service.UserService.getUser(criteria, projection, option, function (err, result) {
        if (err) {
            console.log("err",err);
            callback(err);
        } else {
            console.log("result");
            callback(null,result);
        }
    });
};

var updateUser = function (data,callback) {
    console.log("data", data);
    var accessToken =data.accessToken;
    var dataToUpdate = {};
    if (data.name && data.name != '') {
        dataToUpdate.name = data.name;
    }
    if (data.email && data.email != '') {
        dataToUpdate.email = data.email;
    }
    if (Object.keys(dataToUpdate).length == 0) {
        callback(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.NOTHING_TO_UPDATE);
    }
    else {
        async.auto({
            validEmail:function (cb) {
                if(data.email){

                    if(UniversalFunctions.verifyEmailFormat(data.email)){
                        cb(null);
                    }
                    else{
                        cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_EMAIL);
                    }
                }
                else {
                    cb(null);
                }
            },
            checkEmail:['validEmail',function (cb) {
                if(data.email){
                    var criteria = {
                        email: data.email
                    };
                    var projection = {};
                    var option = {
                        lean: true
                    };
                    Service.UserService.getUser(criteria, projection, option, function (err, result) {
                        if (err) {
                            console.log("err",err);
                            cb(err);
                        } else {
                            console.log("vauds",result)
                            if(result.length){
                                cb(UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.EMAIL_EXIST);
                            }
                            else{
                                cb(null);
                            }
                        }
                    });
                }
                else{
                    cb(null);
                }

            }],
            updateUser:['checkEmail',function (cb) {
                var criteria = {
                    accessToken:accessToken
                };
                var setQuery = {
                    $set: dataToUpdate
                };
                Service.UserService.updateUser(criteria, setQuery, {new: true},  function (err, result) {
                    if (err) {
                        console.log('err144-----', err);
                        cb(err);
                    } else {
                        console.log("created",result);
                        cb(null);
                    }
                });
            }]
        }, function (err, result) {
            if (err) {
                callback(err);
            }
            else {
                callback(null,accessToken);
            }
        });
    }
   

};

module.exports={
    addUser:addUser,
    deleteUser:deleteUser,
    getUsers:getUsers,
    updateUser:updateUser
}
/**
 * Created by prince on 10/7/15.
 */
module.exports  = {
    AdminController : require('./AdminController'),
    AppVersionController : require('./AppVersionController'),
    CustomerController : require('./CustomerController'),
    UserController : require('./UserController'),

};
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Customers = new Schema({
    name: {type: String, trim: true, default: null, sparse: true},
    countryCode: {type: String, trim: true, min:2, max:5},
    facebookId: {type: String, default: null, trim: true, index: true},
    email: {type: String, trim: true, unique: true, index: true, required: true},
    codeUpdatedAt: {type: Date, default: Date.now, required: true},
    registrationDate : {type: Date, default: Date.now, required: true},
    appVersion : {type: String},
    accessToken : {type: String, trim: true, index: true, unique: true, sparse: true},
    deviceToken : {type: String, trim: true},
    deviceType: {
        type: String, enum: [
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.IOS,
            Config.APP_CONSTANTS.DATABASE.DEVICE_TYPES.ANDROID
        ]
    },
    isBlocked: {type: Boolean, default: false},
    totalReviews: {type: Number, default: 0},
    Rating:{type:Number,default:0},
    isDeleted:{type:Boolean,default:0},
    latitude: {type: Number,default:0},
    longitude:{type:Number,default:0},
    profilePicURL: {
        original: {type: String, default: null},
        thumbnail: {type: String, default: null}
    },
    categories:[{type:Schema.ObjectId,ref:'Categories'}],
    aboutMe:{type:String,default:'',trim:true},
    sessions:{type:Number,default:0},
    timings:{type:Schema.ObjectId,ref:'Timings'},
    level:{type:String,default:'',trim:true},
    goodFor:{type:String,default:'',trim:true},
    distanceMeter:{type:Number,default:2000},
    serachCategories:[{type:Schema.ObjectId,ref:'Categories'}],
    serachLatitude : {type: Number,default:0},
    serachLongitude :{type:Number,default:0},
    serachAddress : {type:String,default:'',trim:true},
    serachTime:{type:Schema.ObjectId,ref:'Timings'},
    blockedCustomers:[{type : Schema.ObjectId,ref:'Customers' }],
    homeLatitude: {type: Number,default:0},
    homeLongitude:{type:Number,default:0},
    homeAddress:{type:String,default:'',trim:true},
    workLatitude: {type: Number,default:0},
    workLongitude:{type:Number,default:0},
    workAddress:{type:String,default:'',trim:true}
});

Customers.index({'currentLocation.coordinates': "2d"});


module.exports = mongoose.model('Customers', Customers);
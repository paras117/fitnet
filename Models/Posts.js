var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Posts = new Schema({
    customer: {type: Schema.ObjectId, ref: 'Customers', required: true},
    locationLongLat: {type: [Number], index: '2d', default: [0, 0], required: true},
    locationAddress:{type:String,default:null,required:true},
    creationDate:{type: Date, default: Date.now, required: true},
    title:{type:String,default:null,trim:true,required:true},
    description: {type: String, default: null, trim: true, required: true},
    postDate:{type:String,required:true},
    timings: {type: Schema.ObjectId, ref:'Timings',required:true},
    categories:[{type:Schema.ObjectId,ref:'Categories',required:true}],
    categoryName:{type:String,default:'',required:false},
    sessionTime:{type:String,default:'9:30 am',required:false},
    acceptUser:[
        {
            acceptedCustomer:{type:Schema.ObjectId,ref:'Customers'},
            acceptTime:{type: Date,default:''},
            startTime:{type: Date,default:''},
            endTime:{type: Date,default:''}
        }
    ],
    isDeleted : {type : Boolean, default:false}
});


module.exports = mongoose.model('Posts', Posts);

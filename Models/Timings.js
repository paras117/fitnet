var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Timings = new Schema({
    categoryName: {type: String, default: null, trim: true, required: true},
    Timing: {type: String, default: null, trim: true, required: true, unique: true},
    creationDate:{type: Date, default: Date.now, required: true},
    isDeleted : {type : Boolean, default:false},
    order:{type:Number,default:0}
});


module.exports = mongoose.model('Timings', Timings);



module.exports = {
    Customers : require('./Customers'),
    AppVersions : require('./AppVersions'),
    Posts: require('./Posts'),
    Categories : require('./Categories'),
    Timings: require('./Timings'),
    PostComments:require('./PostComments'),
    Reviews:require('./Reviews')
};
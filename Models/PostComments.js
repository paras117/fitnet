var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var PostComments = new Schema({
    post: {type: Schema.ObjectId,ref:'Posts', required: true},
    comment:[{
        customer: {type: Schema.ObjectId, ref: 'Customers', required: true},
        comment:{type:String,default:null,trim: true,required:true},
        creationDate:{type: Date, default: Date.now, required: true},
    }],
    isDeleted : {type : Boolean, default:false}
});


module.exports = mongoose.model('PostComments', PostComments);

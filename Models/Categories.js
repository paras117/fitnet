var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Categories = new Schema({
    name: {type: String, default: null, trim: true,unique:true, required: true},
    creationDate:{type: Date, default: Date.now, required: true},
    updatedOn: {type: Date, default: Date.now, required: true},
    isDeleted : {type : Boolean, default:false},
    order:{type:Number,default:0}
});


module.exports = mongoose.model('Categories', Categories);


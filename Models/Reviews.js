var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Reviews = new Schema({
    fromCustomer:{type:Schema.ObjectId,ref:'Customers'},
    toCustomer:{type:Schema.ObjectId,ref:'Customers'},
    review:{type:String,default:''},
    rating:{type:String,default:''},
    comments:[{
        customer: {type: Schema.ObjectId, ref: 'Customers'},
        comment:{type:String,default:''},
        creationDate:{type: Date, default: Date.now}
    }],
    creationDate:{type: Date, default: Date.now},
    isDeleted : {type : Boolean, default:false}
});


module.exports = mongoose.model('Reviews', Reviews);


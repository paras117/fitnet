'use strict';
/**
 * Created by prince on 12/7/15.
 */
var mongoose = require('mongoose');
var Config = require('../Config');
var SocketManager = require('../Lib/SocketManager');
var Service = require('../Services');
var async = require('async');

//Connect to MongoDB
mongoose.connect(Config.dbConfig.mongo.URI, function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});
exports.bootstrapAdmin = function (callback) {
    var adminData1 = {
        email: 'princesingla@outlook.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        name: 'Shahab'
    };
    var adminData2 = {
        email: 'prince@code-brew.com',
        password: '1e7eebb19ca71233686f26a43bbc18a9',
        name: 'Click Labs'
    };
    async.parallel([
        function (cb) {
            insertData(adminData1.email, adminData1, cb)
        },
        function (cb) {
            insertData(adminData2.email, adminData2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })


};

exports.bootstrapAppVersion = function (callback) {
    var appVersion1 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.CUSTOMER
    };
    var appVersion2 = {
        latestIOSVersion: '100',
        latestAndroidVersion: '100',
        criticalAndroidVersion: '100',
        criticalIOSVersion: '100',
        appType: Config.APP_CONSTANTS.DATABASE.USER_ROLES.DRIVER
    };


    async.parallel([
        function (cb) {
            insertVersionData(appVersion1.appType, appVersion1, cb)
        },
        function (cb) {
            insertVersionData(appVersion2.appType, appVersion2, cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished For App Version');
    })


};




exports.bootstrapCategory = function (callback) {
    var category1 = {
        name: 'GYM',
        order:1
    };
    var category2 = {
        name: 'Running',
        order:2
    };
    var category3 = {
        name: 'Swimming',
        order:3
    };
    var category4 = {
        name: 'Cycling',
        order:4
    };
    var category5 = {
        name: 'General workout',
        order:5
    };
    var category6 = {
        name: 'Abs',
        order:6
    };
    var category7 = {
        name: 'Weight training',
        order:7
    };
    var category8 = {
        name: 'Yoga',
        order:8
    };
    var category9 = {
        name: 'Boxing',
        order:9
    };
    var category10 = {
        name: 'Others',
        order:10
    };
    var category11 = {
        name: 'Any of the above',
        order:11
    };
    async.parallel([
        function (cb) {
            insertCategory(category1,cb)
        },
        function (cb) {
            insertCategory(category2,cb)
        },

        function (cb) {
            insertCategory(category3,cb)
        },

        function (cb) {
            insertCategory(category4,cb)
        },

        function (cb) {
            insertCategory(category5,cb)
        },

        function (cb) {
            insertCategory(category6,cb)
        },

        function (cb) {
            insertCategory(category7,cb)
        },

        function (cb) {
            insertCategory(category8,cb)
        },

        function (cb) {
            insertCategory(category9,cb)
        },

        function (cb) {
            insertCategory(category10,cb)
        },

        function (cb) {
            insertCategory(category11,cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })


};


exports.bootstrapTiming = function (callback) {
    var timing1 = {
        categoryName:"Weekday",
        Timing:"Early Morning (5:30-9:00)",
        order:1
    };
    var timing2 = {
        categoryName:"Weekday",
        Timing:"Mid Morning (9:00-12:00)",
        order:2
    };
    var timing3 = {
        categoryName:"Weekday",
        Timing:"Lunch (12:00-14:30)",
        order:3
    };
    var timing4 = {
        categoryName:"Weekday",
        Timing:"Afternoon (14:30-18:00)",
        order:4
    };
    var timing5 = {
        categoryName:"Weekday",
        Timing:"Evening (18:00-21:00)",
        order:5
    };
    var timing6 = {
        categoryName:"Weekday",
        Timing:"Late (21:00-23:00)",
        order:6
    };
    var timing7 = {
        categoryName:"Weekday",
        Timing:"Early Morning (5:30-9:00)",
        order:7
    };
    var timing8 = {
        categoryName:"Weekend",
        Timing:"Mid Morning (9:00-12:00)",
        order:8
    };
    var timing9 = {
        categoryName:"Weekend",
        Timing:"Lunch (12:00-14:30)",
        order:9
    };
    var timing10 = {
        categoryName:"Weekend",
        Timing:"Afternoon (14:30-18:00)",
        order:10
    };
    var timing11 = {
        categoryName:"Weekend",
        Timing:"Evening (18:00-21:00)",
        order:11
    };
    var timing12 = {
        categoryName:"Weekend",
        Timing:"Late (21:00-23:00)",
        order:12
    };
    var timing13 = {
        categoryName:"Any",
        Timing:"Any",
        order:13
    };
    async.parallel([
        function (cb) {
            insertTiming(timing1,cb)
        },
        function (cb) {
            insertTiming(timing2,cb)
        },

        function (cb) {
            insertTiming(timing3,cb)
        },

        function (cb) {
            insertTiming(timing4,cb)
        },

        function (cb) {
            insertTiming(timing5,cb)
        },

        function (cb) {
            insertTiming(timing6,cb)
        },

        function (cb) {
            insertTiming(timing7,cb)
        },

        function (cb) {
            insertTiming(timing8,cb)
        },

        function (cb) {
            insertTiming(timing9,cb)
        },
        function (cb) {
            insertTiming(timing10,cb)
        },

        function (cb) {
            insertTiming(timing11,cb)
        },

        function (cb) {
            insertTiming(timing12,cb)
        },
        function (cb) {
            insertTiming(timing13,cb)
        }
    ], function (err, done) {
        callback(err, 'Bootstrapping finished');
    })


};




function insertTiming(timing,callback){
    var obj = {
        categoryName:timing.categoryName,
        Timing:timing.Timing,
        order:timing.order
    };
    Service.CategoryService.createTiming(obj,function(err,data){
        console.log("insert Timing ",err,data);
        callback(err,data);
    })
}





function insertCategory(category,callback){
    var obj = {
        name : category.name,
        order:category.order
    };
    Service.CategoryService.createCategories(obj,function(err,data){
            console.log("insert category ",err,data);
            callback(err,data);    
    })
}






function insertVersionData(appType, versionData, callback) {
    var needToCreate = true;
    async.series([
        function (cb) {
        var criteria = {
            appType: appType
        };
        Service.AppVersionService.getAppVersion(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AppVersionService.createAppVersion(versionData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + appType);
        callback(err, 'Bootstrapping finished For Admin Data')
    })
}

function insertData(email, adminData, callback) {
    var needToCreate = true;
    async.series([function (cb) {
        var criteria = {
            email: email
        };
        Service.AdminService.getAdmin(criteria, {}, {}, function (err, data) {
            if (data && data.length > 0) {
                needToCreate = false;
            }
            cb()
        })
    }, function (cb) {
        if (needToCreate) {
            Service.AdminService.createAdmin(adminData, function (err, data) {
                cb(err, data)
            })
        } else {
            cb();
        }
    }], function (err, data) {
        console.log('Bootstrapping finished for ' + email);
        callback(err, 'Bootstrapping finished')
    })
}

//Start Socket Server
exports.connectSocket = SocketManager.connectSocket;


